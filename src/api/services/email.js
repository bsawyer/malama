import nodemailer from 'nodemailer';
import {
  SMTP_HOST,
  SMTP_PORT,
  SMTP_USER,
  SMTP_PASSWORD,
} from '../config';

const SMTP = {
  host: SMTP_HOST,
  port: SMTP_PORT,
  auth: {
    user: SMTP_USER,
    pass: SMTP_PASSWORD
  }
};

export function sendVerification(email, url, next){
  let transporter = nodemailer.createTransport(SMTP);

  let message = {
    from: 'Malama <noreply@malama.org>',
    to: `<${email}>`,
    subject: `Verify your Malama account`,
    text: `Please confirm your email address to verify your account. If you received this by mistake, please disregard this email. \nConfirm email address: ${url}`
  };

  transporter.sendMail(message, next);
}

export function sendPasswordReset(email, url, next){
  let transporter = nodemailer.createTransport(SMTP);

  let message = {
    from: 'Malama <noreply@malama.org>',
    to: `<${email}>`,
    subject: `Reset your Malama account password`,
    text: `A request was submitted to reset your password. If you did not request this, please ignore this email. \nIt will expire in 1 hour. \nReset password: ${url}`
  };

  transporter.sendMail(message, next);
}