import mongodb from 'mongodb';
import {apiState} from '../../state';
import {CLEANUP_MAX_ACTIVE_DAYS} from '../../../lib';

export function createCleanup(cleanup, next){
  apiState.db.collection('cleanups').insertOne(cleanup, (err, results)=>{
    next(err, results.ops[0]);
  });
}

export function getCleanupById(cleanupId, next){
  try{
    cleanupId = new mongodb.ObjectID(cleanupId);
  }catch(e){
    return next(e);
  }
  apiState.db.collection('cleanups').findOne({'_id': cleanupId}, null, next);
}

export function searchCleanups(query, next){
  let status = query.status || 'pending'; // pending, active, complete, cancelled
  let sort = query.sort || 'create'; // create, start, complete
  let limit = query.limit || 10;
  let start = query.start;
  let radius;
  let coordinates;
  let sortBy = {};

  const cursorQuery = {
    '$query': {}
  };

  switch(status){
  case 'pending':
    cursorQuery['$query'].startDate = {'$gte': new Date()};
    cursorQuery['$query'].completeDate = null;
    break;
  case 'active':
    cursorQuery['$query'].startDate = {'$lt': new Date(), '$gte': new Date(new Date().getTime() - CLEANUP_MAX_ACTIVE_DAYS * 86400000)};
    // cursorQuery['$query'].startDate = {'$lt': new Date()};
    cursorQuery['$query'].completeDate = null;
    break;
  case 'complete':
    cursorQuery['$query'].completeDate = {'$lt': new Date()};
    break;
  case 'cancelled':
    cursorQuery['$query'].startDate = null;
    break;
  }

  if(query.coordinates){
    radius = query.radius || 10; // miles
    coordinates = query.coordinates.split(',').map(l => parseFloat(l));
    cursorQuery['$query'].location = {
      $geoWithin: {
        $centerSphere: [coordinates, radius / 3963.2] // converts the distance to radians
      }
    };
  }

  if(start){
    if(sort === 'create'){
      try {
        start = new mongodb.ObjectID(start);
      }catch(e){
        console.log(e);
        next(e);
        return;
      }
      cursorQuery['$query']['_id'] = {
        '$lt': start
      };
    }else{
      start = new Date(start);
      cursorQuery['$query'][sort + 'Date'] = {
        '$lt': start
      };
    }
  }

  sortBy[sort === 'create' ? '_id' : sort + 'Date'] = -1;

  apiState.db.collection('cleanups')
    .find(cursorQuery)
    .sort(sortBy)
    .limit(limit)
    .toArray((err, results)=>{
      if(err){
        next(err);
        return;
      }
      next(null, results || []);
    });
}

export function updateCleanupById(cleanupId, cleanup, next){
  try{
    cleanupId = new mongodb.ObjectID(cleanupId);
  }catch(e){
    return next(e);
  }
  apiState.db.collection('cleanups').findOneAndUpdate({'_id': cleanupId}, {$set: cleanup}, {returnOriginal: false}, (err, result)=>{
    if(err || result.lastErrorObject && result.lastErrorObject.updatedExisting === false){
      err = err || `Failed to update cleanup with id '${cleanupId}'`;
    }
    next(err, result && result.value);
  });
}

export function getUserCleanups(userId, next){
  apiState.db.collection('cleanups').find({'participants.userId':{$eq: userId}})
    .toArray((err, results)=>{
      if(err){
        next(err);
        return;
      }
      next(null, results || []);
    });
}