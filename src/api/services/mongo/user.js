import mongodb from 'mongodb';
import {apiState} from '../../state';

export function getUserById(userId, next){
  try{
    userId = new mongodb.ObjectID(userId);
  }catch(e){
    return next(e);
  }
  apiState.db.collection('users').findOne({'_id': userId}, null, next);
}

export function getUserByTagName(name, next){
  apiState.db.collection('users').findOne({tag: {name}}, null, next);
}

export function getUserByEmail(email, next){
  apiState.db.collection('users').findOne({email}, null, next);
}

export function createUser(user, next){
  apiState.db.collection('users').insertOne(user, (err, results)=>{
    next(err, results.ops[0]);
  });
}

export function updateUserById(userId, user, next){
  try{
    userId = new mongodb.ObjectID(userId);
  }catch(e){
    return next(e);
  }
  apiState.db.collection('users').findOneAndUpdate({'_id': userId}, {$set: user}, {returnOriginal: false}, (err, result)=>{
    if(err || result.lastErrorObject && result.lastErrorObject.updatedExisting === false){
      err = err || `Failed to update user with id '${userId}'`;
    }
    next(err, result && result.value);
  });
}

export function deleteUserById(userId, next){
  try{
    userId = new mongodb.ObjectID(userId);
  }catch(e){
    return next(e);
  }
  apiState.db.collection('users').deleteOne({'_id': userId}, null, next);
}