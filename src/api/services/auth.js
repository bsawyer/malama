import jwt from 'jsonwebtoken';
import {
  APP_URL,
  TOKEN_SECRET,
  TOKEN_EXPIRATION,
  VERIFY_TOKEN_EXPIRATION,
  RESET_TOKEN_EXPIRATION
} from '../config';

export function generateToken(user){
  return jwt.sign(
    {
      _id: user._id,
      email: user.email,
      permission: user.permission,
      type: 'auth'
    },
    TOKEN_SECRET,
    {
      expiresIn: TOKEN_EXPIRATION
    }
  );
}

export function generateVerifyTokenLink(user){
  const token = jwt.sign(
    {
      _id: user._id,
      email: user.email,
      type: 'verify'
    },
    TOKEN_SECRET,
    {
      expiresIn: VERIFY_TOKEN_EXPIRATION
    }
  );
  return `https://${APP_URL}/verify?token=${token}`;
}

export function generateResetTokenLink(user){
  const token = jwt.sign(
    {
      _id: user._id,
      email: user.email,
      type: 'reset'
    },
    TOKEN_SECRET,
    {
      expiresIn: RESET_TOKEN_EXPIRATION
    }
  );
  return `https://${APP_URL}/reset?token=${token}`;
}

// decoded is null === missing token
// decoded name === error verifying
export function decodeTokenRequest(req, secret, next){
  let token = req.body.token || req.query.token || req.headers['x-access-token'];
  if(token){
    jwt.verify(
      token,
      secret,
      (err, decoded)=>{
        req.decoded = err || decoded;
        next();
      }
    );
  }else{
    req.decoded = null;
    next();
  }
}

export function authorizeMiddleware(req, res, next){
  if(req.permission && (req.decoded.permission < req.permission)){
    return res.status(403).send({error: `Authenticated user is not permitted`});
  }
  next();
}

export function authenticateMiddleware(req, res, next){
  if(!req.decoded || req.decoded.name){
    if(!req.decoded){
      return res.status(401).send({error: `Missing token`});
    }else if(req.decoded.name === 'TokenExpiredError'){
      return res.status(401).send({error: `Token expired`});
    }
    return res.status(401).send({error: `Failed to authenticate token`});
  }
  next();
}