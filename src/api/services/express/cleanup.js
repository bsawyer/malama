import workflow from '@bsawyer/workflow';

import sharp from 'sharp';
import crypto from 'crypto';
import fs from 'fs';
import path from 'path';
import {IMAGE_PATH} from '../../config';

import {
  validateCreateCleanupRequest,
  validateGetCleanupRequest,
  validateSearchCleanupRequest,
  validateUpdateCleanupRequest,
  validateUpdateCleanupImageRequest
} from '../../validators/request';

import {
  Cleanup
} from '../../../lib/values';

import {
  createCleanupWorkflow,
  updateCleanupWorkflow,
  updateCleanupImagesWorkflow
} from '../../workflows/cleanup';

import {
  formatCleanupResponse
} from '../../formatters/response';

import {
  getCleanupById,
  searchCleanups,
  getUserCleanups
} from '../mongo/cleanup';

export function validateSearchCleanup(request, response, next){
  const error = validateSearchCleanupRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateCreateCleanup(request, response, next){
  const error = validateCreateCleanupRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateGetCleanup(request, response, next){
  const error = validateGetCleanupRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateUpdateCleanup(request, response, next){
  const error = validateUpdateCleanupRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateUpdateParticipant(request, response, next){}

export function validateUpdateCleanupImage(request, response, next){
  const error = validateUpdateCleanupImageRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function searchCleanup(request, response){
  searchCleanups(request.query, (error, results)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to get cleanups`});
      return;
    }
    if(!results){
      response.status(404).send({error: `Cleanups not found`});
      return;
    }
    response.status(200).send(results);
  });
}

export function createCleanup(request, response){
  // should this be a format method?
  const cleanup = new Cleanup(
    Object.assign(
      {},
      request.body,
      {participants: [{userId: request.decoded._id, creator: true, joined: true}]}
    )
  );
  const context = {
    type: 'create',
    cleanup
  };
  let stop = workflow(createCleanupWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped cleanup create');
      });
      return;
    }
    response.status(200).send(context.result);
  });
}

export function getCleanup(request, response){
  getCleanupById(request.params.id, (error, result)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to get cleanup`});
      return;
    }
    if(!result){
      response.status(404).send({error: `Cleanup not found`});
      return;
    }
    response.status(200).send(formatCleanupResponse(result));
  });
}

// only creator can update startDate, completeDate, and location
export function updateCleanup(request, response){
  // delete request.body.images;
  delete request.body._id;
  // delete request.body.participants;
  if(request.body.startDate){
    request.body.startDate = new Date(request.body.startDate);
  }
  if(request.body.completeDate){
    request.body.completeDate = new Date(request.body.completeDate);
  }
  const context = {
    type: 'update',
    id: request.params.id,
    cleanup: request.body
  };
  let stop = workflow(updateCleanupWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped update cleanup');
      });
      return;
    }
    response.status(200).send(context.result);
  });
}

export function updateParticipant(request, response){}

export function updateCleanupImage(request, response){
  sharp(request.file.buffer)
    .withMetadata()
    .rotate()
    .resize(500, 500)
    .toBuffer()
    .then((data)=>{
      const hash = crypto.createHash('sha1');
      hash.setEncoding('hex');
      hash.write(data);
      hash.end();
      const filename = hash.read().toString() + path.extname(request.file.originalname);
      fs.writeFile(path.resolve(__dirname, IMAGE_PATH, filename), data, (e) => {
        if (e){
          console.log(e);
          response.status(500).send({error: `Failed to update cleanup images`});
          return;
        }
        const context = {
          id: request.params.id,
          image: filename
        };
        let stop = workflow(updateCleanupImagesWorkflow, context, (error)=>{
          if(error){
            console.log(error);
            response.status(context.status || 500).send({error});
            stop && stop(()=>{
              console.log('stopped update cleanup images');
            });
            return;
          }
          response.status(200).send(context.result);
        });

      });
    })
    .catch((e)=>{
      if (e){
        console.log(e);
        response.status(500).send({error: `Failed to update cleanup images`});
        return;
      }
    });
}

export function getCleanupsByUserId(request, response){
  getUserCleanups(request.params.id, (error, result)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to get cleanups`});
      return;
    }
    if(!result){
      response.status(404).send({error: `Cleanups not found`});
      return;
    }
    response.status(200).send(formatCleanupResponse(result));
  });
}