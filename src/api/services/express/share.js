import Element from '@bsawyer/element';
import mongodb from 'mongodb';

import {
  getCleanupById
} from '../mongo/cleanup';

import {
  getUserById
} from '../mongo/user';

import {
  Location,
  Cleanup,
  User,
  formatCleanupTime
} from '../../../lib';

export function shareCleanup(request, response){
  let cleanup;
  let user;
  console.log('here')
  getCleanupById(request.params.id, (error, result)=>{
    if(error){
      console.log(error)
      response.status(404).send({error: `Cleanup not found`});
    }
    if(!result){
      response.status(404).send({error: `Cleanup not found`});
      return;
    }
    cleanup = result;
    let userId;
    cleanup.participants.some(p => {
      if(p.creator){
        userId = p.userId;
        return true;
      }
    });
    getUserById(userId, (err, res)=>{
      if(err){
        console.log(err)
        response.status(404).send({error: `Cleanup not found`});
      }
      if(!res){
        response.status(404).send({error: `Cleanup not found`});
        return;
      }
      user = res;
      const publicURL = `https://malama.cc/cleanup/${cleanup._id}`;
      const appURL = `https://app.malama.cc/cleanup/${cleanup._id}`;
      const oEmbedURL = `https://app.malama.cc/api/oembed?url=${publicURL}&format=json`;
      const image = cleanup.images && cleanup.images.length ? 'https://app.malama.cc/static/' + cleanup.images[0] : 'https://malama.cc/share-placeholder.png';
      const participants = cleanup.participants.length > 1 ? `${cleanup.participants.length} participants` : `${cleanup.participants.length} participant`;
      const userDetails = user.image && user.username ? `<div class="user-pic" style="background-image: url(https://app.malama.cc/static/${user.image});"></div><div class="user-name">${user.username}</div>` : '';
      const cleanupDate = formatCleanupTime(cleanup);
      const imageCount = cleanup.images && cleanup.images.length > 1 ? `<span class="image-count">1/${cleanup.images.length}</span>` : '';
      response.send(`
        <!DOCTYPE html>
        <html lang="en" dir="ltr">
          <head>
            <meta charset="utf-8">
            <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, viewport-fit=cover">
            <title>Malama - Cleanup</title>
            <meta name="title" content="Malama - Cleanup">
            <meta name="description" content="Show your support by celebrating community cleanups">
            <meta property="og:type" content="website">
            <meta property="og:url" content="${publicURL}">
            <meta property="og:title" content="Malama - Cleanup">
            <meta property="og:description" content="Show your support by celebrating community cleanups">
            <meta property="og:image" content="${image}">
            <meta property="twitter:card" content="summary_large_image">
            <meta property="twitter:url" content="${publicURL}">
            <meta property="twitter:title" content="Malama - Cleanup">
            <meta property="twitter:description" content="Show your support by celebrating community cleanups">
            <meta property="twitter:image" content="${image}">
            <link rel="alternate" type="application/json+oembed" href="${oEmbedURL}" title="Malama - Cleanup" />
            <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" >
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
            <link href="https://fonts.googleapis.com/css?family=Nunito+Sans|Varela+Round&display=swap" rel="stylesheet">
            <link href="/share.css" rel="stylesheet">
          </head>
          <body>
            <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" display="none">
              <symbol id="icon-logo" viewbox="0 0  100 100"><path d="M50,71 C36.181717,61.5431683 29.181717,52.1676436 29,42.873426 C29,24.7246181 56.9320388,27.4394506 56.9320388,40.6399055 C56.7570876,50.5016684 42.9373452,50.6072018 42.9320388,40.6399055 C42.9320388,27.4394506 71,24.8380391 71,42.873426 C70.8991544,49.303522 66.9824964,56.3788793 59.2500261,64.0994978" fill-rule="nonzero"></path></symbol>
              <symbol id="icon-pin" viewbox="0 0  100 100"><path d="M51.1909959,70.5177274 C51.1541434,70.5615155 51.1135998,70.6020591 51.0698117,70.6389116 C50.6472609,70.9945342 50.0164267,70.9402782 49.6608018,70.5177293 C39.8869339,58.9044192 35,50.2865182 35,44.6640264 C35,36.0130235 41.9064098,29 50.425898,29 C58.9453863,29 65.8517961,36.0130235 65.8517961,44.6640264 C65.8517961,50.2865179 60.9648627,58.9044182 51.1909959,70.5177274 Z M50.425898,48.2823725 C53.4077189,48.2823725 55.8249623,45.8651291 55.8249623,42.8833082 C55.8249623,39.9014873 53.4077189,37.4842439 50.425898,37.4842439 C47.4440772,37.4842439 45.0268337,39.9014873 45.0268337,42.8833082 C45.0268337,45.8651291 47.4440772,48.2823725 50.425898,48.2823725 Z"></path></symbol>
            </svg>
            <div class="card">
              <a class="logo-link" href="https://malama.cc">
                <icon class="logo">
                  <svg>
                    <use xlink:href="#icon-logo"></use>
                  </svg>
                </icon>
              </a>
              <a class="cleanup-image" href="${appURL}" style="background-image: url(${image});">${imageCount}</a>
              <div class="card-details">
                <div class="user-date">
                  <div class="user-details">${userDetails}</div>
                  <p class="cleanup-date">${cleanupDate}</p>
                </div>
                <div class="cleanup-details">
                  <p class="participants">${participants}</p>
                  <a class="pin-link" href="https://app.malama.cc?marker=${cleanup._id}">
                    <icon class="pin">
                      <svg>
                        <use xlink:href="#icon-pin"></use>
                      </svg>
                    </icon>
                    Show on map
                  </a>
                </div>
              </div>
            </div>
            <script type="text/javascript" src="/share.js"></script>
          </body>
        </html>
      `);
    });
  });
}

export function oembedCleanup(request, response){
  let width = 500;
  if(!request.query || !request.query.url || request.query.url.match(/cleanup\/.*$/) === null){
    response.status(404).send({error: `Cleanup not found`});
    return;
  }
  if(request.query && request.query.format && request.query.format !== 'json'){
    response.status(501).send({error: `Format not supported`});
    return;
  }
  if(request.query && request.query.maxwidth){
    width = parseFloat(request.query.maxwidth);
  }
  const cleanupId = request.query.url.slice(request.query.url.indexOf('cleanup/') + 8);
  try{
    const i = new mongodb.ObjectID(cleanupId);
  }catch(e){
    response.status(404).send({error: `Cleanup not found`});
    return;
  }
  getCleanupById(cleanupId, (error, result)=>{
    if(error){
      console.log(error);
      response.status(404).send({error: `Cleanup not found`});
      return;
    }
    if(!result){
      response.status(404).send({error: `Cleanup not found`});
      return;
    }
    response.send({
      version: '1.0',
      provider_url: 'https://malama.cc',
      provider_name: 'Malama',
      type: 'rich',
      title: 'Malama - Cleanup',
      html: `<div style='position:relative;padding-bottom: calc(100% + 160px)'><iframe src='https://malama.cc/cleanup/${cleanupId}' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0;' allowfullscreen></iframe></div>`,
      height: null,
      width
    });
  });
}