import jwt from 'jsonwebtoken';
import workflow from '@bsawyer/workflow';

import {authenticateUserWorkflow} from '../../workflows/user';

import {TOKEN_SECRET} from '../../config';
import {apiState} from '../../state';

import {
  validateAuthorizedRequest,
  validateCreateUserRequest,
  validateAuthenticateUserRequest,
  validateAuthenticatedRequest,
  validateUpdateUserRequest
} from '../../validators/request';

import {
  formatAuthenticatedUserResponse
} from '../../formatters/response';

export function validateAuthenticated(request, response, next){
  let error = validateAuthenticatedRequest(request);
  if(!error){
    return next();
  }
  response.status(401).send({error});
}

export function validateAuthenticateUser(request, response, next){
  let error = validateAuthenticateUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateAuthorized(request, response, next){
  let error = validateAuthorizedRequest(request);
  if(!error){
    return next();
  }
  response.status(403).send({error});
}

export function decodeToken(request, response, next){
  // decoded === null: missing token
  // decoded.name: error verifying
  let token = request.body.token || request.query.token || request.headers['x-access-token'];
  if(token){
    jwt.verify(
      token,
      TOKEN_SECRET,
      (err, decoded)=>{
        request.decoded = err || decoded;
        next();
      }
    );
  }else{
    request.decoded = null;
    next();
  }
}

export function authenticateUser(request, response){
  const context = {
    user: request.body
  };
  let stop = workflow(authenticateUserWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(401).send({error: `Failed to authenticate user`});
      stop && stop(()=>{
        console.log('stopped authenticate user');
      });
      return;
    }
    response.status(200).send(formatAuthenticatedUserResponse(context.result, context.token));
  });
}