import workflow from '@bsawyer/workflow';

import sharp from 'sharp';
import crypto from 'crypto';
import fs from 'fs';
import path from 'path';
import {IMAGE_PATH} from '../../config';

import {
  createUserWorkflow,
  updateUserWorkflow,
  createPasswordResetWorkflow,
  verifyUserWorkflow,
  createVerifyUserWorkflow
} from '../../workflows/user';

import {
  validateGetUserRequest,
  validateSearchUserRequest,
  validateCreateUserRequest,
  validateUpdateUserRequest,
  validateAuthorizedUserRequest,
  validateUpdateUserImageRequest,
  validateCreatePasswordResetRequest,
  validateVerifyUserRequest
} from '../../validators/request';

import {
  getUserById,
  getUserByTagName,
  deleteUserById
} from '../mongo/user';

import {
  formatUserResponse,
  formatUserSearchResponse,
  formatAuthenticatedUserResponse
} from '../../formatters/response';

export function validateGetUser(request, response, next){
  const error = validateGetUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateSearchUser(request, response, next){
  const error = validateSearchUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateCreateUser(request, response, next){
  const error = validateCreateUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateUpdateUser(request, response, next){
  const error = validateUpdateUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateDeleteUser(request, response, next){
  const error = validateAuthorizedUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateUpdateUserImage(request, response, next){
  const error = validateUpdateUserImageRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateCreatePasswordReset(request, response, next){
  const error = validateCreatePasswordResetRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateVerifyUser(request, response, next){
  const error = validateVerifyUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function validateCreateUserVerify(request, response, next){
  const error = validateAuthorizedUserRequest(request);
  if(!error){
    return next();
  }
  response.status(400).send({error});
}

export function getUser(request, response){
  getUserById(request.params.id, (error, result)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to get user`});
      return;
    }
    if(!result){
      response.status(404).send({error: `User not found`});
      return;
    }
    response.status(200).send(formatUserResponse(result));
  });
}

export function searchUser(request, response){
  getUserByTagName(request.query.tag, (error, result)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to find user`});
      return;
    }
    response.status(200).send(formatUserSearchResponse(result));
  });
}

export function createUser(request, response){
  const context = {
    type: 'create',
    user: request.body,
    sendVerification: true
  };
  let stop = workflow(createUserWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped user create');
      });
      return;
    }
    response.status(200).send(formatAuthenticatedUserResponse(context.result, context.token));
  });
}

// only properties that can change on user are password and email
export function updateUser(request, response){
  // use a format method here instead
  delete request.body.verified;
  delete request.body.permission;
  delete request.body._id;
  const context = {
    type: 'update',
    id: request.params.id,
    user: request.body
  };
  let stop = workflow(updateUserWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped update user');
      });
      return;
    }
    response.status(200).send(formatUserResponse(context.result));
  });
}

export function deleteUser(request, response){
  // what about comments, posts, tags, etc.?
  deleteUserById(request.params.id, (error, result)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to delete user`});
      return;
    }
    response.status(200).send();
  });
}

// need to make this a workflow plz
export function updateUserImage(request, response){
  console.log('trying to resize and write image');
  sharp(request.file.buffer)
    .withMetadata()
    .rotate()
    .resize(500, 500)
    .toBuffer()
    .then((data)=>{
      const hash = crypto.createHash('sha1');
      hash.setEncoding('hex');
      hash.write(data);
      hash.end();
      const filename = hash.read().toString() + path.extname(request.file.originalname);
      console.log('writing...')
      fs.writeFile(path.resolve(__dirname, IMAGE_PATH, filename), data, (e) => {
        if (e){
          console.log(e);
          response.status(500).send({error: `Failed to update user image`});
          return;
        }
        console.log('wrote image to' + path.resolve(__dirname, IMAGE_PATH, filename));
        const context = {
          type: 'verify',
          id: request.params.id,
          user: {image: filename}
        };
        let stop = workflow(verifyUserWorkflow, context, (error)=>{
          if(error){
            console.log(error);
            response.status(context.status || 500).send({error});
            stop && stop(()=>{
              console.log('stopped update user image');
            });
            return;
          }
          response.status(200).send(formatUserResponse(context.result));
        });

      });
    })
    .catch((e)=>{
      if (e){
        console.log(e);
        response.status(500).send({error: `Failed to update user image`});
        return;
      }
    });
}

export function createUserPasswordReset(request, response){
  const context = {
    type: 'createPasswordReset',
    user: request.body
  };
  let stop = workflow(createPasswordResetWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(500).send({error: `Failed to create password reset`});
      stop && stop(()=>{
        console.log('stopped user password reset');
      });
      return;
    }
    response.status(200).send();
  });
}

export function verifyUser(request, response){
  const context = {
    type: 'verify',
    id: request.params.id,
    user: {verified: true}
  };
  let stop = workflow(verifyUserWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped verify user');
      });
      return;
    }
    response.status(200).send(formatUserResponse(context.result));
  });
}

//this function uses the tokens embeded email address
export function createUserVerify(request, response){
  const context = {
    id: request.params.id,
    sendVerification: true,
    user: {
      email: request.decoded.email
    }
  };
  let stop = workflow(createVerifyUserWorkflow, context, (error)=>{
    if(error){
      console.log(error);
      response.status(context.status || 500).send({error});
      stop && stop(()=>{
        console.log('stopped create verify user');
      });
      return;
    }
    response.status(200).send();
  });
}