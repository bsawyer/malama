import {
  requestHasParams,
  requestHasBody,
  requestBodyHasProperty,
  requestIsAuthorized,
  requestUserIsAuthorizedToModifyUser,
  requestHasValidId,
  requestHasValidUserQuery,
  requestHasValidCleanupQuery
} from '../policies';

// these policies should be called from a specific validator
import {
  userHasValidEmail,
  userHasValidPassword,
  userPasswordsMatch,
  cleanupHasValidLocation,
  cleanupHasValidStartDate
} from '../../lib/policies';

const createUserRequiredProperties = ['email', 'password', 'confirmPassword'];
const createCleanupRequiredProperties = ['location', 'startDate'];

export function validateRequestParams(request){
  if(requestHasParams(request)){
    return;
  }
  return `Request missing params`;
}

export function validateRequestBodyHasProperty(request, property){
  if(!requestBodyHasProperty(request, property)){
    return `Request body missing property ${property}`;
  }
}

export function validateRequestBody(request, properties){
  if(!requestHasBody(request)){
    return `Request missing body`;
  }
  let error;
  properties.some(property => {
    error = validateRequestBodyHasProperty(request, property);
    if(error){
      return true;
    }
  });
  if(error) return error;
}

export function validateCreateUserRequest(request){
  let error = validateRequestBody(request, createUserRequiredProperties);
  if(error) return error;
  // now treat as createUserRequest
  if(!userHasValidEmail(request.body)){
    return `Invalid email`;
  }
  if(!userHasValidPassword(request.body)){
    return `Invalid password. Minimum eight characters, at least one letter and one number`;
  }
  if(!userPasswordsMatch(request.body)){
    return `Passwords do not match`;
  }
}

export function validateAuthorizedRequest(request){
  if(!requestIsAuthorized(request)){
    return `Authenticated user is not permitted`;
  }
}

export function validateAuthenticatedRequest(request){
  if(!request.decoded || request.decoded.name){
    if(!request.decoded){
      return `Missing token`;
    }else if(request.decoded.name === 'TokenExpiredError'){
      return `Token expired`;
    }
    return `Failed to authenticate token`;
  }
}

export function validateAuthenticateUserRequest(request){
  if(!userHasValidEmail(request.body) || !userHasValidPassword(request.body)){
    return `Failed to authenticate user`;
  }
}

export function validateUpdateUserRequest(request){
  if(!requestHasBody(request)){
    return `Request missing body`;
  }
  if(!requestHasParams(request, 'id')){
    return `Request missing user id`;
  }
  if(requestBodyHasProperty(request, 'email') && !userHasValidEmail(request.body)){
    return `Invalid email`;
  }
  if(requestBodyHasProperty(request, 'password')){
    if(!userHasValidPassword(request.body)){
      return `Invalid password. Minimum eight characters, at least one letter and one number`;
    }
    if(!userPasswordsMatch(request.body)){
      return `Passwords do not match`;
    }
  }
  if(!requestUserIsAuthorizedToModifyUser(request)){
    return `Authenticated user is not permitted`;
  }
}

export function validateGetUserRequest(request){
  if(!requestHasValidId(request)){
    return `Invalid user id`;
  }
}

export function validateSearchUserRequest(request){
  if(!requestHasValidUserQuery(request)){
    return `Invalid user query`;
  }
}

export function validateAuthorizedUserRequest(request){
  if(!requestHasParams(request, 'id')){
    return `Request missing user id`;
  }
  if(!requestUserIsAuthorizedToModifyUser(request)){
    return `Authenticated user is not permitted`;
  }
}

export function validateUpdateUserImageRequest(request){
  if(!request.file){
    return `Request missing file`;
  }
}

export function validateUpdateCleanupImageRequest(request){
  if(!request.file){
    return `Request missing file`;
  }
}

export function validateCreatePasswordResetRequest(request){
  if(requestBodyHasProperty(request, 'email') && !userHasValidEmail(request.body)){
    return `Invalid email`;
  }
}

export function validateVerifyUserRequest(request){
  if(request.decoded.type !== 'verify'){
    return `Invalid Token`;
  }
  if(!requestHasParams(request, 'id')){
    return `Request missing user id`;
  }
  if(!requestUserIsAuthorizedToModifyUser(request)){
    return `Authenticated user is not permitted`;
  }
}

export function validateCreateCleanupRequest(request){
  let error = validateRequestBody(request, createCleanupRequiredProperties);
  if(error) return error;
  if(!cleanupHasValidLocation(request.body)){
    return `Invalid location`;
  }
  if(!cleanupHasValidStartDate(request.body)){
    return `Invalid start date`;
  }
}

export function validateGetCleanupRequest(request){
  if(!requestHasValidId(request)){
    return `Invalid cleanup id`;
  }
}

export function validateSearchCleanupRequest(request){
  if(!requestHasValidCleanupQuery(request)){
    return `Invalid cleanup query`;
  }
}

export function validateUpdateCleanupRequest(request){
  if(!requestHasBody(request)){
    return `Request missing body`;
  }
  if(!requestHasParams(request, 'id')){
    return `Request missing user id`;
  }
  // who can modify cleanup props and who can modify participants array
  // if(!requestUserIsAuthorizedToModifyCleanup(request)){
  //   return `Authenticated user is not permitted`;
  // }
}