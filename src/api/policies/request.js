import {mongoIdIsValid} from './mongo';

export function requestHasParams(request){
  return !!request.params;
}

export function requestHasBody(request){
  return !!request.body && !requestBodyEmpty(request);
}

export function requestBodyEmpty(request){
  return !Object.keys(request.body).filter(k => k !== '_id').length;
}

export function requestHasQuery(request){
  return !!request.query;
}

export function requestHasParam(request, param){
  return requestHasParams(request) && !!request.params[param];
}

export function requestHasQueryParam(request, param){
  return requestHasQuery(request) && !!request.query[param];
}

export function requestBodyHasProperty(request, property){
  return requestHasBody(request) && !!request.body[property];
}

export function requestUserIsAuthorizedToModifyUser(request){
  return request.decoded._id === request.params.id;
}

export function requestIsAuthorized(request){
  return request.permission ? (request.decoded.permission && request.decoded.permission >= request.permission) : true;
}

export function requestHasValidUserQuery(request){
  return requestHasQueryParam(request, 'tag');
}

export function requestHasValidId(request){
  return requestHasParam(request, 'id') && mongoIdIsValid(request.params.id);
}

export function requestHasValidCleanupQuery(request){
  return true; // :)
}