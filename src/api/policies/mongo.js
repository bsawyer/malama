import mongodb from 'mongodb';

export function mongoIdIsValid(id){
  try{
    const i = new mongodb.ObjectID(id);
  }catch(e){
    return false;
  }
  return true;
}