export const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://mongo:27017';
export const MONGODB_NAME = process.env.MONGODB_NAME || 'malama';
export const MONGODB_RETRY_DELAY = process.env.MONGODB_RETRY_DELAY || 1000;
export const PORT = process.env.PORT || 5000;
export const SMTP_HOST = process.env.SMTP_HOST || 'smtp.ethereal.email';
export const SMTP_PORT = process.env.SMTP_PORT || 587;
export const SMTP_USER = process.env.SMTP_USER || 'qdckhjqmxjr4drpr@ethereal.email';
export const SMTP_PASSWORD = process.env.SMTP_PASSWORD || 'eg6DHVs77hNXMnqbVt';
export const TOKEN_SECRET = process.env.TOKEN_SECRET || 'wakeupguys';
export const TOKEN_EXPIRATION = process.env.TOKEN_EXPIRATION || '99 days';
export const VERIFY_TOKEN_EXPIRATION = process.env.VERIFY_TOKEN_EXPIRATION || '10 days';
export const RESET_TOKEN_EXPIRATION = process.env.RESET_TOKEN_EXPIRATION || '1 hour';
export const APP_URL = process.env.APP_URL || 'REPLACE_APP_URL';
export const IMAGE_PATH = process.env.IMAGE_PATH || './images'; //relative to server file(__dirname) where to save images
export const FILESIZE_LIMIT = process.env.FILESIZE_LIMIT || 50000000; // 50 * 1024 * 1024 52428800 50 MB
export const SUPPORTED_IMAGE_MIMETYPES = [
  'image/jpeg',
  'image/gif',
  'image/png',
  'image/bmp',
  'image/webp',
  'image/svg+xml'
];
