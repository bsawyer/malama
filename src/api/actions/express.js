import {PORT} from '../config';
import {default as app} from '../express';

export default function start(state){
  state.server = app.listen(PORT);
  console.log(`listening on port ${PORT}`);
  return function stop(){
    state.server.close();
    delete state.server;
  };
}