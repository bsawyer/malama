import {getUserByEmail} from '../../services/mongo/user';

export default function start(context, next){
  // new user or updating a user
  getUserByEmail(context.user.email, (err, result)=>{
    if(result && (context.type === 'create' || context.id !== result._id.toString())){
      context.status = 409;
      next(`User with email '${context.user.email}' exists`);
      return;
    }
    if(!result && context.type === 'update'){
      context.user.verified = false;
      context.sendVerification = true;
    }
    context.result = result;
    next();
  });

  return function stop(){
    // mongodb db.killOp()
  };
}