import {getUserById} from '../../services/mongo/user';

export default function start(context, next){
  getUserById(context.id, (err, result)=>{
    if(err){
      next(err);
      return;
    }
    if(!result){
      context.status = 404;
      next(`User not found`);
      return;
    }
    context.result = result;
    next();
  });
}