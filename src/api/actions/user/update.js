import {updateUserById} from '../../services/mongo/user';

export default function start(context, next){
  updateUserById(context.id, context.user, (err, result)=>{
    if(err){
      context.status = 400;
      next(err);
      return;
    }
    context.result = result;
    next();
  });
  return function stop(){
    // mongodb db.killOp()
  };
}