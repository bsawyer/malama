import bcrypt from 'bcryptjs';

export default function start(context, next){
  // user update might not have password
  if(!context.user.password){
    next();
    return;
  }
  bcrypt.hash(context.user.password, 7, (err, hash)=>{
    if(err){
      context.status = 500;
    }else{
      delete context.user.confirmPassword;
      context.user.password = hash;
    }
    next(err);
  });
}