import {sendPasswordReset} from '../../services/email';
import {generateResetTokenLink} from '../../services/auth';

export default function start(context){
  sendPasswordReset(context.result.email, generateResetTokenLink(context.result), (err)=>{
    if(err){
      console.log(err);
    }
  });
}