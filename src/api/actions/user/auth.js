import bcrypt from 'bcryptjs';
import {generateToken} from '../../services/auth';

export default function start(context, next){
  bcrypt.compare(context.user.password, context.result.password, function(e, match) {
    if(e){
      next(e);
      return;
    }
    if(!match){
      next(`User provided password does not match`);
      return;
    }
    context.token = generateToken(context.result);
    next();
  });
}