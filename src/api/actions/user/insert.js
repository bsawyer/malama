import {createUser} from '../../services/mongo/user';
import {generateToken} from '../../services/auth';

export default function start(context, next){
  createUser(context.user, (err, result)=>{
    if(err){
      context.status = 500;
    }
    context.result = result;
    context.token = generateToken(result);
    next(err);
  });

  return function stop(){
    // mongodb db.killOp()
  };
}