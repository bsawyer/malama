import {sendVerification} from '../../services/email';
import {generateVerifyTokenLink} from '../../services/auth';

export default function start(context){
  if(context.sendVerification){
    console.log('sending verification')
    sendVerification(context.user.email, generateVerifyTokenLink(context.user), (err)=>{
      if(err){
        console.log(err);
      }
    });
  }
  // don't prevent the workflow from finishing
  // as it would block the response
  // could have a response action?
  // this should error but service calling workflow needs to check error
}