import {getUserByEmail} from '../../services/mongo/user';

export default function start(context, next){
  getUserByEmail(context.user.email, (err, result)=>{
    if(err){
      next(err);
      return;
    }
    if(!result){
      next(`User with email '${context.user.email}' does not exist`);
      return;
    }
    context.result = result;
    next();
  });
}