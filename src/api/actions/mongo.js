import {
  MONGODB_URI,
  MONGODB_NAME,
  MONGODB_RETRY_DELAY
} from '../config';

import mongodb from 'mongodb';

function retryConnection(next){
  console.log(`trying to connect to mongo at '${MONGODB_URI}'`);
  mongodb.MongoClient.connect(MONGODB_URI, { useNewUrlParser: true }, (err, mongoClient)=>{
    if(err){
      console.log(err);
      setTimeout(()=>{
        retryConnection(next);
      }, MONGODB_RETRY_DELAY);
      return;
    }
    next(null, mongoClient);
  });
}

export default function action(state, next){
  retryConnection((err, mongoClient)=>{
    state.mongoClient = mongoClient;
    state.db = mongoClient.db(MONGODB_NAME);
    state.db.collection('cleanups').createIndex({'location': '2dsphere'});
    next();
  });
  return function stop(cb){
    if(state.mongoClient){
      state.mongoClient.close((err)=>{
        delete state.db;
        delete state.mongoClient;
        cb();
      });
      return;
    }
    cb();
  };
}