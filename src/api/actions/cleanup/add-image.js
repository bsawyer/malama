export default function start(context){
  context.cleanup = context.result;
  if(!context.cleanup.images){
    context.cleanup.images = [];
  }
  if(context.cleanup.images.indexOf(context.image) === -1){
    context.cleanup.images.push(context.image);
  }
}