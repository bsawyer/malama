import {updateCleanupById} from '../../services/mongo/cleanup';

export default function start(context, next){
  updateCleanupById(context.id, context.cleanup, (err, result)=>{
    if(err){
      context.status = 400;
      next(err);
      return;
    }
    context.result = result;
    next();
  });
  return function stop(){
    // mongodb db.killOp()
  };
}