import {getCleanupById} from '../../services/mongo/cleanup';

export default function start(context, next){
  getCleanupById(context.id, (err, result)=>{
    if(err){
      context.status = 500;
    }
    context.result = result;
    next(err);
  });

  return function stop(){
    // mongodb db.killOp()
  };
}