import {createCleanup} from '../../services/mongo/cleanup';

export default function start(context, next){
  createCleanup(context.cleanup, (err, result)=>{
    if(err){
      context.status = 500;
    }
    context.result = result;
    next(err);
  });

  return function stop(){
    // mongodb db.killOp()
  };
}