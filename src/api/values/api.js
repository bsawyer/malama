export default class Api{
  constructor({
    mongoClient,
    db,
    server
  } = {}){
    this.mongoClient = mongoClient;
    this.db = db;
    this.server = server;
  }
}