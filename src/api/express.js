import {
  SUPPORTED_IMAGE_MIMETYPES,
  FILESIZE_LIMIT,
  IMAGE_PATH
} from './config';

import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';

import {
  notFound
} from './services/express/util';

import {
  validateAuthenticated,
  validateAuthenticateUser,
  validateAuthorized,
  decodeToken,
  authenticateUser
} from './services/express/auth';

import {
  validateGetUser,
  validateSearchUser,
  validateCreateUser,
  validateUpdateUser,
  validateDeleteUser,
  validateUpdateUserImage,
  validateCreatePasswordReset,
  validateVerifyUser,
  validateCreateUserVerify,
  getUser,
  searchUser,
  createUser,
  updateUser,
  deleteUser,
  updateUserImage,
  createUserPasswordReset,
  verifyUser,
  createUserVerify
} from './services/express/user';

import {
  validateSearchCleanup,
  validateCreateCleanup,
  validateGetCleanup,
  validateUpdateCleanup,
  validateUpdateParticipant,
  validateUpdateCleanupImage,
  searchCleanup,
  createCleanup,
  getCleanup,
  updateCleanup,
  updateParticipant,
  updateCleanupImage,
  getCleanupsByUserId
} from './services/express/cleanup';

import {
  shareCleanup,
  oembedCleanup
} from './services/express/share';

const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();
const fileUpload = multer({
  storage: multer.memoryStorage(),
  fileFilter: (req, file, cb) => {
    if (!SUPPORTED_IMAGE_MIMETYPES.includes(file.mimetype)) {
      return cb(null, false);
    }
    return cb(null, true);
  },
  limits: {
    fileSize: FILESIZE_LIMIT,
  }
}).single('image'); //this is the field name

const app = express();
const router = express.Router();
const api = express.Router();
const auth = express.Router();
const user = express.Router();
const cleanup = express.Router();
const share = express.Router();
const oembed = express.Router();

auth.post('/', validateAuthenticateUser, authenticateUser);

user.get('/', validateSearchUser, searchUser);
user.post('/', validateCreateUser, createUser);
user.get('/:id', validateGetUser, getUser);
user.put('/:id', validateAuthenticated, validateUpdateUser, updateUser);
user.delete('/:id', validateAuthenticated, validateDeleteUser, deleteUser);
user.put('/:id/image', validateAuthenticated, fileUpload, validateUpdateUserImage, updateUserImage);
user.put('/:id/verify', validateAuthenticated, validateVerifyUser, verifyUser);
user.post('/:id/verify', validateAuthenticated, validateCreateUserVerify, createUserVerify);
user.post('/password', validateCreatePasswordReset, createUserPasswordReset);

cleanup.get('/', validateAuthenticated, validateSearchCleanup, searchCleanup);
cleanup.get('/user/:id', validateAuthenticated, validateSearchCleanup, getCleanupsByUserId);
cleanup.post('/', validateAuthenticated, validateCreateCleanup, createCleanup);
cleanup.get('/:id', validateAuthenticated, validateGetCleanup, getCleanup);
cleanup.put('/:id', validateAuthenticated, validateUpdateCleanup, updateCleanup);
// cleanup.put('/:id/participant', validateAuthenticated, validateUpdateParticipant, updateParticipant);
cleanup.put('/:id/image', validateAuthenticated, fileUpload, validateUpdateCleanupImage, updateCleanupImage);

oembed.get('/', oembedCleanup);

share.get('/:id', validateGetCleanup, shareCleanup);

api.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
});
api.use(urlencodedParser, jsonParser, decodeToken, validateAuthorized);
api.use('/auth', auth);
api.use('/user', user);
api.use('/cleanup', cleanup);
api.use('/oembed', oembed);

router.use('/api', api);

router.use('/share', share);

router.use(notFound);

app.use(router);

export default app;