import workflow from '@bsawyer/workflow';
import api from './workflows/api';
import { apiState } from './state';

console.log(`starting api...`);
const stop = workflow(api, apiState, e =>{
  if(e){
    console.log(e);
    console.log(`stopping...`);
    stop(() => console.log(`stopped`));
    return;
  }
  console.log(`started`);
});