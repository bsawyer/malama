import {
  mongo,
  express
} from '../actions';

export default {
  concurrent: true,
  actions: [
    mongo,
    express
  ]
};