import {
  insertCleanupAction
} from '../../actions/cleanup';

export default {
  concurrent: false,
  actions: [
    insertCleanupAction
  ]
};