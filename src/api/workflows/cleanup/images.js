import {
  getCleanupAction,
  addImageAction,
  updateCleanupAction
} from '../../actions/cleanup';

export default {
  concurrent: false,
  actions: [
    getCleanupAction,
    addImageAction,
    updateCleanupAction
  ]
};