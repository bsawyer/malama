import {
  updateCleanupAction
} from '../../actions/cleanup';

export default {
  concurrent: false,
  actions: [
    updateCleanupAction
  ]
};