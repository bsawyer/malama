import {
  getUserAction,
  sendVerificationEmailAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    getUserAction,
    sendVerificationEmailAction
  ]
};