import {
  userEmailAction,
  authenticateUserAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    userEmailAction,
    authenticateUserAction
  ]
};