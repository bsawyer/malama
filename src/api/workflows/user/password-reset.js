import {
  userEmailAction,
  userPasswordResetAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    userEmailAction,
    userPasswordResetAction
  ]
};