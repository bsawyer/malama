import {
  getUserAction,
  updateUserAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    getUserAction,
    updateUserAction
  ]
};