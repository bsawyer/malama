import {
  uniqueEmailAction,
  hashPasswordAction,
  insertUserAction,
  sendVerificationEmailAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    {
      concurrent: true,
      actions: [
        uniqueEmailAction,
        hashPasswordAction
      ]
    },
    insertUserAction,
    sendVerificationEmailAction
  ]
};