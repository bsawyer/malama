import {
  updateUserAction,
  uniqueEmailAction,
  hashPasswordAction,
  sendVerificationEmailAction
} from '../../actions/user';

export default {
  concurrent: false,
  actions: [
    {
      concurrent: true,
      actions: [
        uniqueEmailAction,
        hashPasswordAction
      ]
    },
    updateUserAction
    // sendVerificationEmailAction
  ]
};