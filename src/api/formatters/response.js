import {User} from '../../lib';

export function formatUserResponse(user){
  return new User({
    _id: user._id,
    email: user.email,
    verified: user.verified,
    permission: user.permission,
    cleanups: user.cleanups,
    image: user.image,
    username: user.username
  });
}

export function formatAuthenticatedUserResponse(user, token){
  return {
    token,
    user: formatUserResponse(user)
  };
}

export function formatUserSearchResponse(result){
  return result ? formatUserResponse(result) : null;
}

export function formatCleanupResponse(cleanup){
  return cleanup;
}