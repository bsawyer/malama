export default class Image{
  constructor({
    userId,
    path
  } = {}){
    this.userId = userId;
    this.path = path;
  }
}
