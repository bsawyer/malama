import Location from './location';
import Participant from './participant';
import Image from './image';

export default class Cleanup{
  constructor({
    _id,
    participants,
    startDate,
    completeDate,
    location,
    images,
    minParticipants,
    maxParticipants,
    name
  } = {}){
    this._id = _id;
    this.participants = participants && participants.map(p => new Participant(p));
    this.startDate = startDate ? new Date(startDate) : startDate;
    this.completeDate = completeDate ? new Date(completeDate) : completeDate;
    this.location = location ? new Location(location): location;
    // this.images = images && images.map(i => new Image(i));
    this.images = images;
    this.minParticipants = minParticipants;
    this.maxParticipants = maxParticipants;
    this.name = name;
  }
}
