export default class Participant{
  constructor({
    userId,
    creator,
    joined
  } = {}){
    this.userId = userId;
    this.creator = creator;
    this.joined = joined;
  }
}
