export default class Preferences{
  constructor({
    emailNotifications = true,
    pushNotifications = true,
    language = 'en',
    country = 'us',
    zoom = 15
  } = {}){
    this.emailNotifications = emailNotifications;
    this.pushNotifications = pushNotifications;
    this.language = language;
    this.country = country;
    this.zoom = zoom;
  }
}