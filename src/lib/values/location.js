export default class Location{
  constructor({
    type,
    coordinates
  } = {}){
    this.type = type || 'Point';
    this.coordinates = coordinates;
  }
}
