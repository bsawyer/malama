import Preferences from './preferences';
import Location from './location';

export default class User{
  constructor({
    _id,
    email,
    password,
    confirmPassword,
    verified = false,
    permission = 0,
    cleanups,
    username,
    image,
    location,
    preferences
  } = {}){
    this._id = _id;
    this.email = email;
    this.password = password;
    this.confirmPassword = confirmPassword;
    this.verified = verified;
    this.permission = permission;
    this.cleanups = cleanups;
    this.username = username;
    this.image = image;
    this.location = location ? new Location(location): location;
    this.preferences = new Preferences(preferences);
  }
}