import * as timeFormat from './time';

// need to translate
function formatCleanupTime(cleanup){
  const now = new Date(); // in users timezone
  if(cleanup.completeDate){
    // how long ago completed?
    return timeFormat.twitter(now, cleanup.completeDate);
  }
  if(now >= cleanup.startDate){
    return `started ${timeFormat.twitter(now, cleanup.startDate)}`;
  }
  return `in ${timeFormat.remaining(timeFormat.mapMilliseconds(cleanup.startDate - now))}`;
}

export {
  formatCleanupTime
};