const FREQUENCY_NAMES = ['year', 'month', 'week', 'day', 'hour', 'minute', 'second'];
const FREQUENCY_VALUES = [31536000000, 2592000000, 604800000, 86400000, 3600000, 60000, 1000];

function mapMilliseconds(ms){
  let i = 0;
  let remainder = ms;
  let map = {};
  while(i < FREQUENCY_NAMES.length){
    map[FREQUENCY_NAMES[i]] = Math.floor(remainder / FREQUENCY_VALUES[i]);
    remainder = remainder % FREQUENCY_VALUES[i];
    i++;
  }
  return map;
}

function remaining(map){
  if(map.year) return map.year + 'y';
  if(map.month) return map.month + 'mo';
  if(map.week) return map.week + 'wk';
  if(map.day) return map.day + 'd';
  if(map.hour) return map.hour + 'h';
  if(map.minute) return map.minute + 'm';
  if(map.second) return map.second + 's';
  return '0s';
}

function twitter(now, date){
  const map = mapMilliseconds(now - date);
  if(map.year) return date.toLocaleDateString('en-US', {year: 'numeric', month: 'short', day: 'numeric'});
  if(map.day) return date.toLocaleDateString('en-US', {month: 'short', day: 'numeric'});
  // will need to translate these
  if(map.hour) return map.hour + 'h';
  if(map.minute) return map.minute + 'm';
  return 'Just now';
}

export {
  mapMilliseconds,
  remaining,
  twitter
};