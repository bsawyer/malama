export function userHasEmail(user){
  return !!user.email;
}

export function userHasPassword(user){
  return !!user.password;
}

export function userHasValidEmail(user){
  return userHasEmail(user) && user.email.match(/(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/g) !== null;
}

export function userPasswordsMatch(user){
  return user.password === user.confirmPassword;
}

export function userHasValidPassword(user){
  return userHasPassword(user) && user.password.match(/^.{8,32}$/g) !== null;
}

export function userIsCleanupCreator(user, cleanup){
  if(!user || !cleanup){
    return false;
  }
  return cleanup.participants.some(p => p.userId === user._id && p.creator);
}