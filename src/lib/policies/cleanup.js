import {Location} from '../values';
import {CLEANUP_MAX_ACTIVE_DAYS} from '../config';

export function cleanupHasValidStartDate(cleanup){
  return cleanup.startDate;
}

export function cleanupHasValidCompleteDate(cleanup){
  return cleanup.completeDate;
}

export function cleanupIsComplete(cleanup){
  return cleanup.completeDate && cleanup.completeDate <= new Date();
}

export function cleanupHasValidLocation(cleanup){
  return cleanup.location && cleanup.location.coordinates;
}

export function cleanupHasMaxImages(cleanup){
  return cleanup.images && cleanup.images.length >= 5;
}

export function cleanupIsActive(cleanup){
  const now = new Date();
  const expired = new Date(new Date().getTime() - CLEANUP_MAX_ACTIVE_DAYS * 86400000);
  return cleanupHasValidStartDate(cleanup) && !cleanupIsComplete(cleanup) && cleanup.startDate < now && cleanup.startDate > expired;
}