import Observer from '@bsawyer/observer';
import Element from '@bsawyer/element';
import {batch} from '../../core';
import {rootState} from '../../state';
import TouchClick from '../touch-click';
import {routeService} from '../../services';

export default class RouteLink extends TouchClick{
  constructor(options){
    super(options);
    this.value = options.value;
    this.observer = new Observer((mutation)=>{
      this.toggleActiveClass(this.isActive(mutation.newValue));
    });
    this.observer.observe(rootState, {
      recursive: false,
      observed: ['route'],
      mutations: ['set']
    });
  }
  isActive(route){
    return Array.isArray(this.value) ?
      this.value.some(v => v === route) :
      this.value === route;
  }
  toggleActiveClass(active){
    batch(()=>{
      this.node.classList.toggle('active', active);
    });
  }
  render(...args){
    super.render(...args);
    Element.inheritAttributes(args[0], this.node);
    Element.inheritChildren(args[0], this.node);
    this.value = this.node.getAttribute('value');
    this.toggleActiveClass(this.isActive(rootState.route));
    this.node.addEventListener('click', (evt)=>{
      evt.preventDefault();
      routeService.activateRoute(rootState, this.value);
    });
  }
  destroy(){
    super.destroy();
    this.observer.disconnect();
  }
}

RouteLink.fragmentId = 'route-link';