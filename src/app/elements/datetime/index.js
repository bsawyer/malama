import Element from '@bsawyer/element';
import TouchClick from '../touch-click';
import {batch} from '../../core';
import Error from '../error';
import {rootState} from '../../state';
import{
  datetimeService
} from '../../services';

function formatNumber(n){
  return n < 10 ? '0' + n : n;
}

const numberStrings = '0123456789';

function inputKeydown(e, elm){
  let char = String.fromCharCode(e.keyCode).trim().toLowerCase();
  if(e.target.getAttribute('name') === 'dd'){
    if(char === 'a' || char === 'p'){
      e.preventDefault();
      e.target.value = char + 'm';
      toggleError(elm);
    }
  }
}

function inputBlur(e, elm){
  let name = e.target.getAttribute('name');
  if(!e.target.value){
    if(name === 'dd'){
      e.target.value = elm.now.dd;
      toggleError(elm);
      return;
    }
    e.target.value = formatNumber(elm.now[name]);
    toggleError(elm);
    return;
  }
  if(name !== 'dd'){
    e.target.value = formatNumber(parseFloat(e.target.value));
    toggleError(elm);
  }
}

function invalidDate(elm){
  // console.log(`checking if date is invalid`);
  if(elm.inputs.some((i)=>{
    let val = i.value.toLowerCase();
    let name = i.getAttribute('name');
    if(name === 'dd'){
      return val !== 'am' && val !== 'pm';
    }
    if(name === 'minute'){
      return false;
    }
    return parseFloat(val) <= 0;
  })){
    return true;
  }
  // const now = elm.getNow();
  const now = elm.validDate || new Date();
  const scheduled = elm.getValue();

  return now > scheduled;
}

function onInput(e, elm){
  let name = e.target.getAttribute('name');

  if(name === 'dd'){
    let v = e.target.value.toLowerCase();
    if(v !== 'am' && v !== 'pm'){
      e.target.value = '';
    }
    return;
  }

  let val = parseFloat(e.target.value);
  switch(name){
    case 'day':
      if(val > 31){
        val = 31;
      }
    break;
    case 'month':
      if(val > 12){
        val = 12;
      }

    break;
    case 'year':
      if(val > 99){
        val = 99;
      }
    break;
    case 'hour':
      if(val > 12){
        val = 12;
      }
    break;
    case 'minute':
      if(val > 59){
        val = 59;
      }
    break;
  }
  if(val <= 0){
    val = 0;
  }
  e.target.value = formatNumber(val);
  toggleError(elm);
}

function toggleError(elm){
  // console.log(`toggling the error`);
  elm.elements.get(Error)[0].toggle(invalidDate(elm));
  elm.onChange && elm.onChange(elm);
}

export default class Datetime extends TouchClick{
  constructor(options){
    super(options);
  }
  render(...args){
    super.render(...args);

    const inputs = Array.prototype.slice.call(this.node.querySelectorAll('input'));

    this.inputs = inputs;
    this.month = inputs[0];
    this.day = inputs[1];
    this.year = inputs[2];
    this.hour = inputs[3];
    this.minute = inputs[4];
    this.dd = inputs[5];

    this.node.addEventListener('click', (e)=>{
      // only has origin on touch events
      let target = e.origin ? e.origin.target : e.target;
      if(this.inputs.some(i => i === target)){
        target.focus();
      }else{
        this.inputs[0].focus();
      }
    });

    this.inputs.forEach((input)=>{
      input.addEventListener('keydown', (e)=>{inputKeydown(e, this)});
      input.addEventListener('blur', (e)=>{inputBlur(e, this)});
      input.addEventListener('input', (e)=>{onInput(e, this)});
    });

    this.elements.get(Error)[0].button.addEventListener('click', ()=>{
      rootState.modal = {
        content: `Invalid date`,
        button: 'OK'
      };
    });

    this.reset();
  }
  isValid(){
    return this.node && !this.elements.get(Error)[0].node.classList.contains('show');
  }
  update(parsedDate){
    parsedDate = parsedDate || this.now;
    this.month.value = formatNumber(parsedDate.month);
    this.day.value = formatNumber(parsedDate.day);
    this.year.value = formatNumber(parsedDate.year);
    this.hour.value = formatNumber(parsedDate.hour);
    this.minute.value = formatNumber(parsedDate.minute);
    this.dd.value = formatNumber(parsedDate.dd);
    const isInvalid = invalidDate(this);
    // console.log(isInvalid);
    this.elements.get(Error)[0].toggle(isInvalid);
  }
  reset(validDate){
    this.validDate = validDate;
    this.now = datetimeService.parse(new Date(new Date().getTime() + 10 * 60000)); //gives 10 min buffer to any update
    this.update();
  }
  getNow(){
    return datetimeService.create(this.now);
  }
  getValue(){
    // this is the value of the users input
    return datetimeService.create({
      month: parseFloat(this.inputs[0].value),
      day: parseFloat(this.inputs[1].value),
      year: parseFloat(this.inputs[2].value),
      hour: parseFloat(this.inputs[3].value),
      minute: parseFloat(this.inputs[4].value),
      second: 59, // hack?
      dd: this.inputs[5].value
    });
  }
  setValue(date){
    this.update(datetimeService.parse(date));
  }
}

Datetime.elements = [
  Error
];