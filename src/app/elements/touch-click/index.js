import Element from '@bsawyer/element';

const TOUCH_PADDING = 10;

export default class TouchClick extends Element{
  constructor(selector){
    super(selector);
    this.startX = 0;
    this.startY = 0;
    this.moved = false;
  }
  render(...args){
    super.render(...args);
    this.node.addEventListener('touchstart', (evt)=>{
      if(evt.touches.length > 1) return;
      this.startX = evt.touches[0].pageX;
      this.startY = evt.touches[0].pageY;
    }, {passive: true});
    this.node.addEventListener('touchmove', (evt)=>{
      if(this.moved || evt.touches.length > 1) return;
      if(Math.abs(evt.touches[0].pageX - this.startX) > TOUCH_PADDING || Math.abs(evt.touches[0].pageY - this.startY) > TOUCH_PADDING){
        this.moved = true;
      }
    }, {passive: true});
    this.node.addEventListener('touchend', (evt)=>{
      // If the touchstart, touchmove or touchend event is canceled during an interaction, no mouse or click events will be fired
      // https://developer.mozilla.org/en-US/docs/Web/API/Touch_events/Supporting_both_TouchEvent_and_MouseEvent
      evt.preventDefault();
      if(evt.touches.length > 1) return;
      if(!this.moved && this.node.contains(evt.target)){
        this.node.focus(); // to hide keyboard?
        this.dispatchClick(this.node, evt);
      }
      this.startX = 0;
      this.startY = 0;
      this.moved = false;
    });
  }
  dispatchClick(node, evt){
    const event = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true
    });
    event.origin = evt;
    node.dispatchEvent(event);
  }
}