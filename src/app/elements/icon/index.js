import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {batch} from '../../core';

export default class Icon extends Element{
  constructor(options){
    super(options);
    this.state = {
      name: options.name || ''
    };
    this.stateObserver = new Observer(()=>{
      if(this.node){
        this.update();
      }
    });
    this.stateObserver.observe(this.state, {
      recursive: false,
      observed: ['name'],
      mutations: ['set']
    });
  }
  update(){
    this.use.setAttributeNS('http://www.w3.org/1999/xlink', 'href', `#icon-${this.state.name}`);
  }
  render(...args){
    super.render(...args);
    Element.inheritAttributes(args[0], this.node);
    this.use = this.node.querySelector('use');
    this.state.name = this.node.getAttribute('name') || this.state.name; // will kick off update
  }
}