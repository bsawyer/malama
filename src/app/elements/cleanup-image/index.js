import Element from '@bsawyer/element';
import Route from '../route';
import Icon from '../icon';
import Button from '../button';
import {rootState} from '../../state';
import {batch} from '../../core';
import {APP_URL} from '../../config';
import {
  imagesService
} from '../../services';

export default class CleanupImage extends Element{
  constructor(options){
    super(options);
    this.data = options.data;
  }
  update(){
    batch(()=>{
      this.img.src = this.data.dataURL || `${APP_URL}/static/${this.data}`
    });
  }
  render(...args){
    super.render(...args);
    this.img = this.node.querySelector('img');

    this.elements.get(Button)[0].node.addEventListener('click', ()=>{
      imagesService.remove(rootState, this.data);
    });

    this.update();
  }
}

CleanupImage.elements = [
  Button
];