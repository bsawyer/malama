import Element from '@bsawyer/element';
import TouchClick from '../touch-click';
import Icon from '../icon';
import {batch} from '../../core';
import {rootState} from '../../state';
import {APP_URL} from '../../config';

import {
  Location,
  Cleanup,
  User,
  formatCleanupTime
} from '../../../lib';

import {
  geoService,
  modalService,
  routeService,
  cleanupService,
  userService,
  spinnerService
} from '../../services';

export default class ListItem extends TouchClick{
  constructor(options){
    super(options);
    this.state = {
      cleanup: options.cleanup
    };
  }
  update(){
    batch(()=>{
      let t = formatCleanupTime(this.state.cleanup);
      this.datetime.textContent = t;
      if(this.state.cleanup.images && this.state.cleanup.images[0]){
        this.img.src = `${APP_URL}/static/${this.state.cleanup.images[0]}`;
      }else{
        this.img.src = '';
      }
    });
  }
  render(...args){
    super.render(...args);

    this.datetime = this.node.querySelector('.datetime');
    this.img = this.node.querySelector('img');

    this.node.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, 'cleanup/:id', {id: this.state.cleanup._id});
    });

    this.update();
  }
}

ListItem.elements = [
  Icon
];