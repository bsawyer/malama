import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {rootState} from '../../state';
import {batch} from '../../core';

export default class Spinner extends Element{
  constructor(options){
    super(options);
    this.showSpinnerObserver = new Observer(()=>{
      this.update();
    });
    this.showSpinnerObserver.observe(rootState, {
      recursive: false,
      observed: ['showSpinner'],
      mutations: ['set']
    });
  }
  update(){
    batch(()=>{
      this.node.classList.toggle('show', !!rootState.showSpinner);
    });
  }
  render(...args){
    super.render(...args);
    Element.inheritAttributes(args[0], this.node);
    Element.inheritChildren(args[0], this.node);
    this.update();
  }
}