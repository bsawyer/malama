import Element from '@bsawyer/element';
import Route from '../route';
import Icon from '../icon';
import Button from '../button';
import {rootState} from '../../state';
import {
  routeService
} from '../../services';

export default class SearchAction extends Route{
  constructor(options){
    super(options);
  }
  render(...args){
    super.render(...args);
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, 'search');
    });
  }
}

SearchAction.elements = [
  Icon,
  Button
];

SearchAction.pattern = /^\s*$/;
SearchAction.transclude = true;