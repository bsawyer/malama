import Observer from '@bsawyer/observer';
import Route from '../route';
import RouteLink from '../route-link';
import Button from '../button';
import {rootState} from '../../state';
import {batch} from '../../core';
import {
  translateService,
  userService,
  routeService,
  modalService,
  spinnerService
} from '../../services';
import {
  userHasValidEmail,
  userPasswordsMatch,
  userHasValidPassword
} from '../../../lib';

const authEmailValidator = {
  isInvalid: function(user){
    return !userHasValidEmail(user);
  }
};

const authPasswordValidator = {
  isInvalid: function(user){
    return !userHasValidPassword(user);
  }
};

const authConfirmPasswordValidator = {
  isInvalid: function(user){
    return !userPasswordsMatch(user);
  }
};

function onSuccess(element, {user, token}){
  // if the client has already given us a location and user signs in, we need to take that location and update the user
  let location;
  if(rootState.user.location){
    location = rootState.user.location;
  }
  // rootState.user = user;
  Object.assign(rootState.user, user);
  rootState.token = token;
  if(location){
    rootState.user.location = location;
    userService.updateUser(rootState.user, rootState.token, (err)=>{
      if(err){
        console.log(err);
      }
    });
  }
  if(rootState.deniedRouteConfig){
    routeService.activateRoute(rootState, rootState.deniedRouteConfig.route, rootState.deniedRouteConfig.params);
  }else{
    routeService.activateRoute(rootState, '');
  }

}

const pageMap = {
  'sign-in': {
    title: 'Sign in to Malama',
    button: 'Sign in',
    validate: [authEmailValidator, authPasswordValidator],
    serviceMethod: 'createToken',
    onSuccess
  },
  'sign-up': {
    title: 'Sign up for Malama',
    button: 'Sign up',
    validate: [authEmailValidator, authPasswordValidator, authConfirmPasswordValidator],
    serviceMethod: 'createUser',
    onSuccess
  },
  'password-reset': {
    title: 'Send reset password link',
    button: 'Send',
    validate: [authEmailValidator],
    serviceMethod: 'createPasswordReset',
    onSuccess: (element, user)=>{
      modalService.passwordResetSuccess(rootState, user);
    }
  },
  'reset': {
    title: 'Reset password',
    button: 'Reset password',
    validate: [authPasswordValidator, authConfirmPasswordValidator],
    serviceMethod: 'resetPassword'
  }
};

export default class Auth extends Route{
  constructor(options){
    super(options);
    this.observer = new Observer(()=>{
      this.translate();
    });
    this.observer.observe(rootState.user.preferences, {
      recursive: false,
      observed: ['language'],
      mutations: ['set']
    });
    this.valid = false;
  }
  routeChange(route){
    super.routeChange(route);
    this.update();
  }
  update(){
    batch(()=>{
      let pageInfo = pageMap[rootState.route];
      if(pageInfo){
        this.title.textContent = translateService.getTranslation(pageInfo.title);
        this.buttonText.textContent = translateService.getTranslation(pageInfo.button);
        this.fields[1].value = '';
        this.fields[2].value = '';
        this.node.setAttribute('route', rootState.route);
        rootState.showError = false;
      }
      this.checkValid();
    });
  }
  translate(){
    translateService.translateElements(this.node, {
      'input[placeholder]': {
        attr: ['placeholder']
      },
      'a': {
        text: true
      },
      'button': {
        text: true
      }
    });
  }
  render(...args){
    super.render(...args);
    this.translate();
    this.title = this.node.querySelector('h1');
    this.button = this.node.querySelector('button');
    this.buttonText = this.button.querySelector('.button-text');
    this.form = this.node.querySelector('form');
    this.fields = Array.prototype.slice.call(this.node.querySelectorAll('input'));
    this.form.addEventListener('submit', (evt)=>{
      let spinnerJob;
      evt.preventDefault();
      if(!this.valid) return;
      let pageInfo = pageMap[rootState.route];
      spinnerJob = spinnerService.add(rootState);
      rootState.showError = false; // trying again?

      userService[pageInfo.serviceMethod](this.getUser(), (err, response)=>{
        spinnerService.remove(spinnerJob, rootState);
        if(err){
          console.log(err);
          rootState.modal = {
            content: err.error || `Something's not right...`,
            button: 'OK'
          }
          rootState.showError = true; // wait until user clicks error button to show modal
          return;
        }
        if(pageInfo.onSuccess){
          pageInfo.onSuccess(this, response);
        }
      });
    });
    this.fields.forEach((input)=>{
      input.addEventListener('keyup', ()=>{
        this.checkValid();
      });
      input.addEventListener('change', ()=>{
        this.checkValid();
      });
    });
    this.update();
  }
  checkValid(){
    let pageInfo = pageMap[rootState.route];
    let user = this.getUser();
    if(pageInfo){
      if(pageInfo.validate.some(v => v.isInvalid(user))){
        this.button.setAttribute('disabled', 'disabled');
        this.valid = false;
        return;
      }
      this.button.removeAttribute('disabled');
      this.valid = true;
    }
  }
  getUser(){
    return this.fields.reduce((prev, f) => {prev[f.getAttribute('name')] = f.value; return prev;}, {});
  }
}

Auth.pattern = /^sign-in|^sign-up|^password-reset|^reset/;
Auth.transclude = true;
Auth.elements = [
  RouteLink,
  Button
];