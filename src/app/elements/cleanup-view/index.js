import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';
import Icon from '../icon';
import ImageAction from '../image-action';
import Datetime from '../datetime';
import BackAction from '../back-action';
import {APP_URL} from '../../config';

import {
  Location,
  Cleanup,
  User,
  formatCleanupTime
} from '../../../lib';

import {
  geoService,
  modalService,
  routeService,
  cleanupService,
  userService,
  spinnerService,
  imagesService,
  shareService
} from '../../services';

export default class CleanupView extends Route{
  constructor(options){
    super(options);
    this.selectedLocationObserver = new Observer((mutation)=>{
      this.update();
    });
    this.selectedLocationObserver.observe(rootState, {
      recursive: false,
      observed: ['cleanup'],
      mutations: ['set']
    });
    this.cleanupCreatorObserver = new Observer((mutation)=>{
      this.update();
    });
    this.cleanupCreatorObserver.observe(rootState, {
      recursive: false,
      observed: ['cleanupCreator'],
      mutations: ['set']
    });
    // observe cleanup and cleanupCreator
  }
  routeChange(route){
    super.routeChange(route);
    if(this.active){
      let spinnerJob = spinnerService.add(rootState);
      cleanupService.get(rootState.routeParams.id, rootState, (err, cleanup)=>{
        if(err){
          console.log(err);
          spinnerService.remove(spinnerJob, rootState);
          return;
        }
        rootState.cleanup = new Cleanup(cleanup);
        imagesService.setPending(rootState);
        userService.getUser(cleanup.participants.find(p => p.creator).userId, rootState.token, (e, user)=>{
          spinnerService.remove(spinnerJob, rootState);
          if(e){
            console.log(e);
            return;
          }
          rootState.cleanupCreator = new User(user);
          if(rootState.cleanup.completeDate){
            rootState.action = null;
            return;
          }
          // set the view action
          if(rootState.cleanupCreator._id === rootState.user._id){
            rootState.action = 'edit';
          }else{
            let participant = rootState.cleanup.participants.find(p => p.userId === rootState.user._id);
            if(participant){
              rootState.action = participant.joined ? 'leave' : 'join';
            }else{
              rootState.action = 'join';
            }
          }
        });
      });
    }else{
      if(rootState.previousRoute && rootState.previousRoute.match(CleanupView.pattern) !== null){
        // console.log('leaving view');
        rootState.cleanup = null;
        rootState.cleanupCreator = null;
        imagesService.clearPending(rootState);
      }
    }
  }
  update(){
    let t;
    let images;
    let pic;
    let name;
    let p;
    if(rootState.cleanup){
      t = formatCleanupTime(rootState.cleanup);
      images = rootState.cleanup.images;
      p = rootState.cleanup.participants.filter(par => par.joined).length;
    }
    if(rootState.cleanupCreator){
      name = rootState.cleanupCreator.username;
      pic = rootState.cleanupCreator.image;
    }
    batch(()=>{
      if(t){
        this.datetime.textContent = t;
      }else{
        this.datetime.textContent = '';
      }
      if(p){
        this.participants.textContent = p + ' ' + (p > 1 ? 'people' : 'person');
      }else{
        this.participants.textContent = '';
      }
      if(images && images[0]){
        this.img.src = `${APP_URL}/static/${images[0]}`;
        if(images.length > 1){
          this.pictureCount.setAttribute('data-count',`1/${images.length}`);
        }else{
          this.pictureCount.removeAttribute('data-count');
        }
      }else{
        this.img.src = '';
        this.pictureCount.removeAttribute('data-count');
      }
      if(name){
        this.userName.textContent = name;
      }else{
        this.userName.textContent = '';
      }
      if(pic){
        this.userPic.setAttribute('style', `background-image:url(${APP_URL}/static/${pic});`);
      }else{
        this.userPic.removeAttribute('style');
      }
    });
  }
  render(...args){
    super.render(...args);

    this.datetime = this.node.querySelector('.datetime');
    this.userPic = this.node.querySelector('.user-pic');
    this.userName = this.node.querySelector('.user-name');
    this.img = this.node.querySelector('img');
    this.pictureCount = this.node.querySelector('picture-count');
    this.showMap = this.node.querySelector('.show-on-map');
    this.getDirections = this.node.querySelector('.get-directions');
    this.share = this.node.querySelector('.share');
    this.participants = this.node.querySelector('.participants');

    this.elements.get(BackAction)[0].button.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, '');
    });

    this.elements.get(ImageAction)[0].button.addEventListener('click', ()=>{
      imagesService.show(rootState, true);
    });

    this.showMap.addEventListener('click', ()=>{
      rootState.showPanel = false;
      rootState.centerCoordinates = rootState.cleanup.location.coordinates;
      routeService.activateRoute(rootState, '');
    });

    this.getDirections.addEventListener('click', ()=>{
      if /* if we're on iOS, open in Apple Maps */
      ((Element.window.navigator.platform.indexOf('iPhone') != -1) ||
       (Element.window.navigator.platform.indexOf('iPad') != -1) ||
       (Element.window.navigator.platform.indexOf('iPod') != -1))
        Element.window.open(`maps://maps.google.com/maps?daddr=${rootState.cleanup.location.coordinates[1]},${rootState.cleanup.location.coordinates[0]}&amp;ll=`);
       else /* else use Google */
        Element.window.open(`https://maps.google.com/maps?daddr=${rootState.cleanup.location.coordinates[1]},${rootState.cleanup.location.coordinates[0]}&amp;ll=`);
    });

    this.share.addEventListener('click', ()=>{
      shareService.share(rootState, rootState.cleanup, (err)=>{
        // ok?
        console.log(err)
      });
    });

    this.update();
  }
}

CleanupView.pattern = /^cleanup\/:id$/;
CleanupView.transclude = true;
CleanupView.elements = [
  Button,
  Icon,
  ImageAction,
  BackAction
];