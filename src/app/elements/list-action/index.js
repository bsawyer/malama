import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';
import Icon from '../icon';
import {
  translateService,
  routeService
} from '../../services';

export default class ListAction extends Element{
  constructor(options){
    super(options);

  }
  update(){

  }
  render(...args){
    super.render(...args);
    this.update();
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      rootState.showPanel = true;
    });
  }
}

ListAction.elements = [
  Button,
  Icon
];