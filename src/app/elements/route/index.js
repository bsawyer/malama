import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {batch} from '../../core';
import {rootState} from '../../state';

export default class Route extends Element{
  constructor(options){
    super(options);
    this.pattern = options.pattern || this.constructor.pattern;
    if(!this.pattern){
      throw new Error('Route element missing pattern');
    }
    this.transclude = typeof options.transclude !== 'undefined' ? options.transclude : this.constructor.transclude;
    this.active = false;
    this.cache = Element.window.document.createDocumentFragment();
    this.observer = new Observer((mutation)=>{
      this.routeChange(mutation.newValue);
    });
    this.observer.observe(rootState, {
      recursive: false,
      observed: ['route'],
      mutations: ['set']
    });
  }
  routeChange(route){
    this.toggleActive(this.shouldMakeActive(route));
  }
  shouldMakeActive(route){
    return route.match(this.pattern) !== null;
  }
  toggleActive(active){
    const lastState = this.active;
    this.active = active;
    if(!this.transclude){
      this.toggleActiveClass(active);
      return;
    }
    if(active && !lastState){
      batch(()=>{
        this.target.parentNode.insertBefore(this.node, this.target);
        this.target.remove();
      });
    }else if(!active && lastState){
      batch(()=>{
        this.node.parentNode.insertBefore(this.target, this.node);
        this.node.remove();
      });
    }
  }
  toggleActiveClass(active){
    batch(()=>{
      this.node.classList.toggle('active', active);
    });
  }
  render(...args){
    if(!this.transclude){
      super.render(...args);
    }else{
      super.render(this.cache, false);
      this.target = args[0];
      this.target.classList.add('route--target');
    }
    // Element.inheritAttributes(args[0], this.node);
    this.routeChange(rootState.route);
  }
}