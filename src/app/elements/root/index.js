import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Auth from '../auth';
import Spinner from '../spinner';
import Error from '../error';
import Modal from '../modal';
import Geo from '../geo';
import Logo from '../logo';
import Action from '../action';
import Panel from '../panel';
import Profile from '../profile';
import CleanupEdit from '../cleanup-edit';
import CleanupView from '../cleanup-view';
import SearchLocation from '../search-location';
import SearchAction from '../search-action';
import Search from '../search';
import Images from '../images';
import {rootState} from '../../state';

export default class Root extends Element{
  constructor(options){
    super(options);
    this.showErrorObserver = new Observer((mutation)=>{
      if(this.node){
        this.elements.get(Error)[0].toggle(mutation.newValue);
      }
    });
    this.showErrorObserver.observe(rootState, {
      recursive: false,
      observed: ['showError'],
      mutations: ['set']
    });
  }
}

Root.selector = 'root';

Root.elements = [
  Auth,
  Spinner,
  Error,
  Modal,
  Geo,
  Logo,
  Action,
  Panel,
  Profile,
  CleanupEdit,
  CleanupView,
  SearchLocation,
  SearchAction,
  Search,
  Images
];
