import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Button from '../button';
import {batch} from '../../core';
import {rootState} from '../../state';

export default class Modal extends Element{
  constructor(options){
    super(options);
    this.config = rootState.modal;
    this.modalConfigObserver = new Observer((mutation)=>{
      this.config = mutation.newValue;
      this.update();
    });
    this.modalConfigObserver.observe(rootState, {
      recursive: false,
      observed: ['modal'],
      mutations: ['set']
    });
    this.showModalObserver = new Observer(()=>{
      batch(()=>{
        this.node.classList.toggle('show', !!rootState.showModal);
      });
    });
    this.showModalObserver.observe(rootState, {
      recursive: false,
      observed: ['showModal'],
      mutations: ['set']
    });
  }
  update(){
    batch(()=>{
      this.node.classList.toggle('show', !!rootState.showModal);
      this.content.textContent = this.config ? this.config.content : '';
      this.button.textContent = this.config ? this.config.button : '';
      if(this.config && this.config.deny){
        this.deny.classList.remove('hide');
        this.deny.textContent = this.config.deny;
      }else{
        this.deny.classList.add('hide');
      }
    });
  }
  render(...args){
    super.render(...args);
    Element.inheritAttributes(args[0], this.node);
    this.content = this.node.querySelector('modal-content');
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      rootState.showModal = false;
      if(this.config && this.config.onDismiss){
        this.config.onDismiss(true);
      }
    });
    this.deny = this.node.querySelector('.inverse');
    this.deny.addEventListener('click', ()=>{
      rootState.showModal = false;
      if(this.config && this.config.onDismiss){
        this.config.onDismiss(false);
      }
    });
    this.update();
  }
}

Modal.elements = [
  Button
];