import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';
import Icon from '../icon';
import BackAction from '../back-action';
import Datetime from '../datetime';
import SearchResult from '../search-result';

import {
  geoService,
  modalService,
  spinnerService,
  routeService
} from '../../services';

export default class SearchLocation extends Route{
  constructor(options){
    super(options);
    this.results = null;
  }
  routeChange(route){
    super.routeChange(route);
  }
  updateSuggestions(suggestions){
    batch(()=>{
      if(this.results){
        this.results.forEach(r => r.destroy());
      }
      this.results = suggestions.map((data)=>{
        let result = new SearchResult({
          data,
          onSelect: (d)=>{
            if(this.onSelect){
              this.onSelect(d);
            }
          }
        });
        result.render(this.suggestions);
        return result;
      });
    });
  }
  onSelect(data){
    if(rootState.route.indexOf('search') !== -1){
      rootState.searchLocation = data;
    }else{
      rootState.selectedLocation = data; // used to create or edit location on cleanup
    }
    this.goBack();
  }
  goBack(){
    let route = rootState.route.indexOf('search') !== -1 ? 'search' : 'cleanup';
    // they could have signed in and hard hit search location url : ()
    if(route === 'cleanup' && rootState.previousParams.id){
      route = route + '/:id/edit';
    }
    routeService.activateRoute(rootState, route, rootState.previousParams);
  }
  render(...args){
    super.render(...args);

    this.input = this.node.querySelector('input');
    this.suggestions = this.node.querySelector('.suggestions');
    const data = {
      place_name: 'Current location', //translate
      currentLocation: true // this one doesnt have geometry
    };
    this.currentLocation = new SearchResult({
      data,
      onSelect: (d)=>{
        if(this.onSelect){
          this.onSelect(d);
        }
      }
    });
    this.currentLocation.render(this.node.querySelector('search-result'), true);

    let d;

    this.input.addEventListener('input', ()=>{
      if(d){
        return;
      }
      let spinnerJob = spinnerService.add(rootState);
      d = setTimeout(()=>{
        d = null;
        if(!this.input.value){
          spinnerService.remove(spinnerJob, rootState);
          return;
        }
        geoService.search(this.input.value, (err, results)=>{
          this.updateSuggestions(results ? results.features || [] : []);
          spinnerService.remove(spinnerJob, rootState);
        });
      }, 1000);
    });

    this.elements.get(BackAction)[0].button.addEventListener('click', ()=>{
      this.goBack();
    });
  }
}

SearchLocation.pattern = /^search\/location|^cleanup\/.*location/;
SearchLocation.transclude = true;
SearchLocation.elements = [
  BackAction
];
