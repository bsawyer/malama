import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import ListAction from '../list-action';
import PinAction from '../pin-action';
import NavigateAction from '../navigate-action';
import {rootState} from '../../state';
import {batch} from '../../core';

import {
  Cleanup
} from '../../../lib';

import {
  geoService,
  modalService,
  cleanupService,
  routeService
} from '../../services';

const COUNTRY_COORDS = {
  us: [-95.7129, 37.0902]
};

// const COUNTRY_BOUNDS = {
//   us: [[-13.334792, -172.934818], [-14.097472, -171.330502]]
// };

export default class Geo extends Route{
  constructor(options){
    super(options);
    this.locationObserver = new Observer(()=>{
      this.updateUserMarker();
    });
    this.locationObserver.observe(rootState.user, {
      recursive: false,
      observed: ['location'],
      mutations: ['set']
    });
    this.setResultsObserver = new Observer((mutation)=>{
      this.updatePins();
    });
    this.setResultsObserver.observe(rootState, {
      recursive: false,
      observed: ['searchResults'],
      mutations: ['set']
    });
    this.centerCoordinatesObserver = new Observer((mutation)=>{
      batch(()=>{
        geoService.centerMap(rootState);
      });
    });
    this.centerCoordinatesObserver.observe(rootState, {
      recursive: false,
      observed: ['centerCoordinates'],
      mutations: ['set']
    });
    this.pins = null;
  }
  updatePins(){
    // hopefully this is never run before we have the map
    if(this.pins){
      this.pins.forEach(p => p.elm.destroy() && p.marker.remove());
    }
    this.pins = rootState.searchResults.map((cleanup)=>{
      const elm = new PinAction();
      let div = Element.window.document.createElement('div');
      elm.render(div);
      elm.button.addEventListener('click', ()=>{
        // this.map.flyTo({
        //   center: cleanup.location.coordinates,
        //   zoom: 15
        // });
        routeService.activateRoute(rootState, 'cleanup/:id', {id: cleanup._id});
      });
      return {
        elm,
        marker: new Element.window.mapboxgl.Marker(elm.node).setLngLat(cleanup.location.coordinates).addTo(this.map)
      }
    });
  }
  updateUserMarker(){
    if(!this.userMarker && rootState.user.location && rootState.user.location.coordinates){
      const userMarkerElm = Element.window.document.createElement('div');
      userMarkerElm.classList.add('user-marker');
      this.userMarker = new Element.window.mapboxgl.Marker(userMarkerElm);
      this.userMarker.setLngLat(rootState.user.location.coordinates);
      geoService.registerUserMarker(this.userMarker);
      return;
    }
    if(rootState.user.location && rootState.user.location.coordinates){
      this.userMarker.setLngLat(rootState.user.location.coordinates);
    }
  }
  routeChange(route){
    super.routeChange(route);
    if(this.active && rootState.softPromptLocation){
      modalService.softPromptLocation(rootState);
    }
    if(this.active){
      rootState.action = 'cleanup';
      rootState.searchResults = [];
      if(rootState.routeParams.marker){
        cleanupService.get(rootState.routeParams.marker, rootState, (err, result)=>{
          if(err){
            console.log(err);
            console.log('failed getting search results');
            // show no results found? need to intercept all the http requests
            return;
          }
          rootState.searchResults = [new Cleanup(result)];
          rootState.centerCoordinates = result.location.coordinates;
        });
      }else{
        cleanupService.search(rootState, (err, results)=>{
          if(err){
            console.log(err);
            console.log('failed getting search results');
            // show no results found? need to intercept all the http requests
            return;
          }
          rootState.searchResults = results.map(r => new Cleanup(r));
        });
      }
      setTimeout(()=>{
        this.map.resize();
        batch(()=>{
          this.node.classList.add('show');
        });
      }, 100); //wtf1?1?!
    }else{
      this.node.classList.remove('show');
    }
  }
  render(...args){
    super.render(...args);

    this.map = new Element.window.mapboxgl.Map({
      container: this.node,
      style: 'mapbox://styles/mapbox/streets-v9',
      center: rootState.user.location ? rootState.user.location.coordinates : COUNTRY_COORDS[rootState.user.preferences.country], // starting position [lng, lat]
      zoom: rootState.user.preferences.zoom, // starting zoom
      attributionControl: false
    });
    this.map.addControl(new Element.window.mapboxgl.AttributionControl({
      compact: false
    }));
    this.map.on('load', ()=>{
      setTimeout(()=>{
        this.map.resize();
        geoService.centerMap(rootState);
        batch(()=>{
          if(this.active){
            batch(()=>{
              this.node.classList.add('show');
            });
          }
        });
      }, 100); //wtf1?1?!
    });
    geoService.registerMap(this.map);

    // if(!rootState.user.location){
    //   map.fitBounds(COUNTRY_BOUNDS[rootState.user.preferences.country]);
    // }
  }
}

Geo.pattern = /^\s*$/;
Geo.transclude = false;
Geo.elements = [
  ListAction,
  NavigateAction
];