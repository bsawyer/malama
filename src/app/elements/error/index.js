import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';

export default class Error extends Element{
  constructor(options){
    super(options);
  }
  toggle(show){
    batch(()=>{
      this.node.classList.toggle('show', !!show);
    });
  }
  render(...args){
    super.render(...args);
    this.toggle(false);
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      rootState.showModal = true;
    });
  }
}

Error.selector = 'error';

Error.elements = [
  Button
];