import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {batch} from '../../core';
import {rootState} from '../../state';
import AddAction from '../add-action';
import BackAction from '../back-action';
import CleanupImage from '../cleanup-image';
import {
  imagesService
} from '../../services';

export default class Images extends Element{
  constructor(options){
    super(options);
    this.showImagesObserver = new Observer(()=>{
      this.updateShow();
    });
    this.showImagesObserver.observe(rootState, {
      recursive: false,
      observed: ['showImages'],
      mutations: ['set']
    });
    this.pendingImagesObserver = new Observer(()=>{
      this.update();
    });
    this.pendingImagesObserver.observe(rootState, {
      recursive: false,
      observed: ['pendingImages'],
      mutations: ['set']
    });
    this.hideActionsObserver = new Observer(()=>{
      this.updateHideImageActions();
    });
    this.hideActionsObserver.observe(rootState, {
      recursive: false,
      observed: ['hideImageActions'],
      mutations: ['set']
    });
    this.images = [];
  }
  updateHideImageActions(){
    batch(()=>{
      this.node.classList.toggle('hide-actions', !!rootState.hideImageActions);
    });
  }
  updateShow(){
    batch(()=>{
      this.node.classList.toggle('show', !!rootState.showImages);
    });
  }
  update(){
    if(this.node){
      batch(()=>{
        this.node.classList.toggle('show', !!rootState.showImages);
        this.images.forEach(i => i.destroy());
        this.images = [];
        if(rootState.pendingImages && rootState.pendingImages.length){
          rootState.pendingImages.forEach((data)=>{
            if(data.fileMapKey){
              data = imagesService.getFile(data.fileMapKey);
            }
            const cleanupImage = new CleanupImage({data});
            this.images.push(cleanupImage);
            cleanupImage.render(this.pending);
          });
          // this.node.scrollTop = this.node.scrollHeight;
        }
        this.elements.get(AddAction)[0].node.classList.toggle('hide', (rootState.pendingImages && rootState.pendingImages.length >= 5));
      });
    }
  }
  render(...args){
    super.render(...args);
    this.input = this.node.querySelector('input');
    this.pending = this.node.querySelector('.pending');

    imagesService.init(rootState, this.input);

    this.elements.get(BackAction)[0].button.addEventListener('click', ()=>{
      imagesService.hide(rootState);
    });

    this.updateShow();
    this.update();
  }
}

Images.elements = [
  AddAction,
  BackAction
];
