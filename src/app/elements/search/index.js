import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';
import Icon from '../icon';
import BackAction from '../back-action';
import Datetime from '../datetime';
import SearchResult from '../search-result';
import {Location, Cleanup} from '../../../lib';

import {
  geoService,
  modalService,
  spinnerService,
  routeService,
  cleanupService
} from '../../services';

export default class Search extends Route{
  constructor(options){
    super(options);
    this.searchLocationObserver = new Observer((mutation)=>{
      this.updateSearchLocation();
    });
    this.searchLocationObserver.observe(rootState, {
      recursive: false,
      observed: ['searchLocation'],
      mutations: ['set']
    });
    this.searchStatusObserver = new Observer((mutation)=>{
      this.updateSearchStatus();
    });
    this.searchStatusObserver.observe(rootState, {
      recursive: false,
      observed: ['searchStatus'],
      mutations: ['set']
    });
  }
  routeChange(route){
    super.routeChange(route);
    if(!this.active){
      this.lastLocation = rootState.searchLocation;
    }else{
      // is this a bad idea?
      if(rootState.previousRoute !== 'search/location'){
        this.updateSearchStatus();
      }
    }
  }
  update(){
    this.updateSearchLocation();
    this.updateSearchStatus();
  }
  updateSearchLocation(){
    batch(()=>{
      this.location.value = rootState.searchLocation ? rootState.searchLocation.place_name : '';
      if(!this.location.value){
        this.saveButton.setAttribute('disabled', 'disabled');
      }else{
        this.saveButton.removeAttribute('disabled');
      }
    });
  }
  updateSearchStatus(){
    batch(()=>{
      this.radios.forEach(r => r.checked = false);
      this.form.querySelector(`input[id=${rootState.searchStatus}]`).checked = true;
    });
  }
  render(...args){
    super.render(...args);

    this.form = this.node.querySelector('form');
    this.radios = Array.prototype.slice.call(this.form.querySelectorAll('input[type=radio]'));
    this.saveButton = this.form.querySelector('button');
    this.cancelButton = this.form.querySelector('.inverse');
    this.location = this.form.querySelector('input[name=location]');

    this.cancelButton.addEventListener('click', ()=>{
      if(rootState.searchLocation !== this.lastLocation){
        rootState.searchLocation = this.lastLocation;
      }
      routeService.activateRoute(rootState, '');
    });

    this.location.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, 'search/location');
    });

    this.form.addEventListener('submit', (evt)=>{
      evt.preventDefault();
      // need to make sure location is selected
      if(!rootState.searchLocation){
        return;
      }
      rootState.searchStatus = this.form.querySelector('[type=radio]:checked').getAttribute('id');
      rootState.centerCoordinates = rootState.searchLocation.currentLocation && rootState.user.location ?
        rootState.user.location.coordinates :
        rootState.searchLocation.geometry.coordinates;
      routeService.activateRoute(rootState, '');
    });

    this.update();
  }
}

Search.pattern = /^search$/;
Search.transclude = true;
Search.elements = [

];
