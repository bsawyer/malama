import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import Button from '../button';
import Icon from '../icon';
import ImageAction from '../image-action';
import Datetime from '../datetime';
import {APP_URL} from '../../config';

import {
  Location,
  Cleanup,
  userIsCleanupCreator,
  cleanupIsActive
} from '../../../lib';

import {
  geoService,
  modalService,
  routeService,
  cleanupService,
  spinnerService,
  imagesService
} from '../../services';

export default class CleanupEdit extends Route{
  constructor(options){
    super(options);
    this.pendingImagesObserver = new Observer(()=>{
      this.updateImages();
    });
    this.pendingImagesObserver.observe(rootState, {
      recursive: false,
      observed: ['pendingImages'],
      mutations: ['set']
    });
    this.cleanupObserver = new Observer(()=>{
      this.update();
    });
    this.cleanupObserver.observe(rootState, {
      recursive: false,
      observed: ['cleanup'],
      mutations: ['set']
    });
  }
  routeChange(route){
    super.routeChange(route);
    if(this.active){
     // check to see if we are creating a cleanup or editing one
     if(rootState.routeParams.id){
       let spinnerJob = spinnerService.add(rootState);
       cleanupService.get(rootState.routeParams.id, rootState, (err, data)=>{
         spinnerService.remove(spinnerJob, rootState);
         if(err){
           console.log(err);
           return;
         }
         const cleanup = new Cleanup(data);
         if(userIsCleanupCreator(rootState.user, cleanup)){
           // format method for this
           rootState.selectedLocation = rootState.selectedLocation || {
             place_name: cleanup.name || cleanup.location.coordinates.join(', '),
             geometry: cleanup.location
           };

           // set form values from cleanup
           rootState.selectedDate = rootState.selectedDate || cleanup.startDate;

           this.isComplete.checked = !!cleanup.completeDate;

           // this.update(); //should observe selectedLocation and selectedDate instead of calling update here
           rootState.cleanup = cleanup;
           imagesService.setPending(rootState);
         }else{
           routeService.activateRoute(rootState, '');
         }
       });
     }else{
       rootState.cleanup = null;
     }
   }else{
     // leaving us and going to a route not location
     if(rootState.previousRoute && rootState.previousRoute.match(CleanupEdit.pattern) !== null && rootState.route.match(/^cleanup\/.*location/) === null){
       // console.log('leaving edit');
       rootState.selectedDate = null;
       rootState.selectedLocation = null;
       rootState.cleanup = null;
       imagesService.clearPending(rootState);
     }
   }
  }
  updateImages(){
    if(this.node){
      batch(()=>{
        if(rootState.pendingImages && rootState.pendingImages[0]){
          if(rootState.pendingImages[0].fileMapKey){
            this.img.src = imagesService.getFile(rootState.pendingImages[0].fileMapKey).dataURL;
          }else{
            this.img.src = `${APP_URL}/static/${rootState.pendingImages[0]}`;
          }
          if(rootState.pendingImages.length > 1){
            this.pictureCount.setAttribute('data-count',`1/${rootState.pendingImages.length}`);
          }else{
            this.pictureCount.removeAttribute('data-count');
          }
        }else{
          this.img.src = '';
          this.pictureCount.removeAttribute('data-count');
        }
      });
    }
  }
  update(){
    batch(()=>{
      this.location.value = rootState.selectedLocation ? rootState.selectedLocation.place_name : '';
    });
    if(rootState.cleanup){
      this.elements.get(Datetime)[0].validDate = rootState.cleanup.startDate;
      if(this.isComplete){
        this.isComplete.classList.toggle('show', cleanupIsActive(rootState.cleanup) || !!rootState.cleanup.completeDate);
      }
    }else{
      // no cleanup?
      if(this.isComplete){
        this.isComplete.classList.remove('show');
      }
    }
    if(rootState.selectedDate){
      this.elements.get(Datetime)[0].setValue(rootState.selectedDate);
    }else{
      this.elements.get(Datetime)[0].reset();
    }
  }
  render(...args){
    super.render(...args);

    this.form = this.node.querySelector('form');
    this.saveButton = this.form.querySelector('button');
    this.cancelButton = this.form.querySelector('.inverse');
    this.location = this.form.querySelector('input[name=location]');
    this.img = this.node.querySelector('img');
    this.pictureCount = this.node.querySelector('picture-count');
    this.isComplete = this.form.querySelector('.is-complete');
    this.completeCheckbox = this.isComplete.querySelector('input[name=complete]');

    this.elements.get(Datetime)[0].onChange = (elm)=>{
      rootState.selectedDate = elm.getValue();
    };

    this.elements.get(ImageAction)[0].button.addEventListener('click', ()=>{
      imagesService.show(rootState);
    });

    this.cancelButton.addEventListener('click', ()=>{
      if(rootState.routeParams.id){
        routeService.activateRoute(rootState, 'cleanup/:id', rootState.routeParams);
        return;
      }
      routeService.activateRoute(rootState, '');
    });

    // any form change enable/ disable submit
    // show errors icons

    this.form.addEventListener('submit', (evt)=>{
      evt.preventDefault();
      if(!this.elements.get(Datetime)[0].isValid()){
        return;
      }
      if(!rootState.selectedLocation){
        return;
      }
      const cleanup = {
        location: rootState.selectedLocation.geometry,
        name: rootState.selectedLocation.place_name,
        startDate: this.elements.get(Datetime)[0].getValue().toUTCString()
      };
      const method = !rootState.routeParams.id ? 'create' : 'update';
      if(method === 'update'){
        // remove the one's we're deletin
        cleanup.images = rootState.pendingImages.filter(p => !p.fileMapKey);
        if(this.isComplete.classList.contains('show')){
          if(this.completeCheckbox.checked && rootState.cleanup.completeDate === null){
            cleanup.completeDate = new Date().toUTCString();
          }
          if(!this.completeCheckbox.checked && rootState.cleanup.completeDate !== null){
            cleanup.completeDate = null;
          }
        }
      }
      let spinnerJob = spinnerService.add(rootState);
      const methodCallback = (err, res)=>{
        if(err){
          console.log(err);
          spinnerService.remove(spinnerJob, rootState);
          // modalService.showError()
          return;
        }
        // start updating images
        // res is the cleanup we either created or updated back from the server
        imagesService.updateCleanup(rootState, res, (e, r)=>{
          if(e){
            console.log(e);
            spinnerService.remove(spinnerJob, rootState);
            // modalService.showError()
            return;
          }
          rootState.cleanup = new Cleanup(r);
          rootState.selectedDate = null;
          rootState.selectedLocation = null;
          spinnerService.remove(spinnerJob, rootState);
          routeService.activateRoute(rootState, 'cleanup/:id', {id: r._id});
          // show update success modal
        });
      };
      if(rootState.selectedLocation.currentLocation){
        geoService.search(rootState.selectedLocation.geometry.coordinates.join(','), (err, res)=>{
          if(err){
            console.log(err);
          }
          if(res && res.features && res.features.length){
            cleanup.name = res.features[0].place_name;
          }
          cleanupService[method](cleanup, rootState, methodCallback);
        });
      }else{
        cleanupService[method](cleanup, rootState, methodCallback);
      }
    });

    this.location.addEventListener('click', ()=>{
      const route = rootState.routeParams.id ? 'cleanup/:id/location' : 'cleanup/location';
      routeService.activateRoute(rootState, route, rootState.routeParams);
    });

    this.location.addEventListener('keydown',()=>{
      // on space -> navigate
    });

    this.update();
  }
}

CleanupEdit.pattern = /^cleanup$|^cleanup\/:id\/edit$/;
CleanupEdit.transclude = true;
CleanupEdit.elements = [
  Button,
  Icon,
  ImageAction,
  Datetime
];