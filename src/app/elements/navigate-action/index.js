import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Icon from '../icon';
import Button from '../button';
import Route from '../route';
import {rootState} from '../../state';
import {batch} from '../../core';
import {
  modalService,
  geoService
} from '../../services';

export default class NavigateAction extends Element{
  constructor(options){
    super(options);
    this.permissionObserver = new Observer(()=>{
      this.update();
    });
    this.permissionObserver.observe(rootState, {
      recursive: false,
      observed: ['locationPermissionGranted'],
      mutations: ['set']
    });
    this.softPromptObserver = new Observer(()=>{
      this.update();
    });
    this.softPromptObserver.observe(rootState, {
      recursive: false,
      observed: ['softPromptLocation'],
      mutations: ['set']
    });
  }
  update(){
    batch(()=>{
      this.node.classList.toggle('show', (rootState.locationPermissionGranted && rootState.user.location) || rootState.softPromptLocation);
    });
  }
  render(...args){
    super.render(...args);

    this.update();
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      if(rootState.softPromptLocation){
        modalService.softPromptLocation(rootState);
      }else{
        if(rootState.user.location){
          rootState.centerCoordinates = rootState.user.location.coordinates;
        }
      }
    });
  }
}

NavigateAction.elements = [
  Icon,
  Button
];
