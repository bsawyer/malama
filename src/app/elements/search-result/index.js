import Element from '@bsawyer/element';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import Observer from '@bsawyer/observer';
import Icon from '../icon';
import {
  routeService
} from '../../services';

export default class SearchResult extends TouchClick{
  constructor(options){
    super(options);
    this.data = options.data; // {place_name, currentLocation, }
    this.onSelect = options.onSelect;
    if(this.data.currentLocation){
      this.data.geometry = rootState.user.location;
      this.locationObserver = new Observer((mutation)=>{
        this.data.geometry = mutation.newValue; //this data object is assigned to selectedLocation in search-location
        this.update();
      });
      this.locationObserver.observe(rootState.user, {
        recursive: false,
        observed: ['location'],
        mutations: ['set']
      });
    }
  }
  update(){
    if(this.data.currentLocation){
      this.node.classList.toggle('hide', !this.data.geometry);
    }
  }
  render(...args){
    super.render(...args);
    if(this.data.currentLocation){
      this.elements.get(Icon)[0].state.name = 'locate';
      this.node.querySelector('.name').textContent = 'Use current location'; //translate this
    }else{
      this.elements.get(Icon)[0].state.name = 'pin';
      this.node.querySelector('.name').textContent = this.data.place_name; // mapbox language support?
    }
    this.node.addEventListener('click', ()=>{
      this.onSelect(this.data);
    });
    this.update();
  }
}

SearchResult.elements = [
  Icon
];