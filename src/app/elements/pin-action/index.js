import Element from '@bsawyer/element';
import Icon from '../icon';
import Button from '../button';

export default class PinAction extends Element{
  constructor(options){
    super(options);
  }
  render(...args){
    super.render(...args);
    this.button = this.node.querySelector('button');
  }
}

PinAction.elements = [
  Icon,
  // Button
];