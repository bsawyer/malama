import Element from '@bsawyer/element';
import Route from '../route';
import Observer from '@bsawyer/observer';
import Icon from '../icon';
import Button from '../button';
import {rootState} from '../../state';
import {batch} from '../../core';
import {
  imagesService
} from '../../services';

export default class AddAction extends Element{
  constructor(options){
    super(options);
    this.showSpinnerObserver = new Observer(()=>{
      this.update();
    });
    this.showSpinnerObserver.observe(rootState, {
      recursive: false,
      observed: ['showSpinner'],
      mutations: ['set']
    });
  }
  update(){
    batch(()=>{
      this.node.classList.toggle('hide', !!rootState.showSpinner);
    });
  }
  render(...args){
    super.render(...args);
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      imagesService.add(rootState);
    });
    this.update();
  }
}

AddAction.elements = [
  Button
];