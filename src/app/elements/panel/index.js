import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import TouchClick from '../touch-click';
import {rootState} from '../../state';
import {batch} from '../../core';
import MapAction from '../map-action';
import List from '../list';

import {
  geoService,
  modalService
} from '../../services';

export default class Panel extends Route{
  constructor(options){
    super(options);
    this.showErrorObserver = new Observer(()=>{
      this.update();
    });
    this.showErrorObserver.observe(rootState, {
      recursive: false,
      observed: ['showPanel'],
      mutations: ['set']
    });
  }
  update(){
    batch(()=>{
      this.node.classList.toggle('show', !!rootState.showPanel);
    });
  }
  render(...args){
    super.render(...args);
    this.update();
  }
}

Panel.pattern = /^\s*$/;
Panel.transclude = true;
Panel.elements = [
  MapAction,
  List
];