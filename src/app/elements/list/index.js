import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {batch} from '../../core';
import ListItem from '../list-item';
import {rootState} from '../../state';

export default class List extends Element{
  constructor(options){
    super(options);
    this.setResultsObserver = new Observer((mutation)=>{
      this.update(mutation);
    });
    this.setResultsObserver.observe(rootState, {
      recursive: false,
      observed: ['searchResults'],
      mutations: ['set']
    });

  }
  update(mutation){
    if(this.node){
      batch(()=>{
        if(!rootState.searchResults.length){
          this.node.innerHTML = '<p class="no-results">No search results</p>';
          return;
        }
        this.node.innerHTML = '';
        rootState.searchResults.forEach((cleanup)=>{
          const listItem = new ListItem({
            cleanup
          });
          listItem.render(this.node);
        });
      });
    }
  }
  render(...args){
    super.render(...args);
    this.update();
  }
}