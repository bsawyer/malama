import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import Icon from '../icon';
import Button from '../button';
import ImageAction from '../image-action';
import {rootState} from '../../state';
import {batch} from '../../core';
import {APP_URL} from '../../config';
import RouteLink from '../route-link';

import {
  routeService,
  spinnerService,
  userService
} from '../../services';

export default class Profile extends Route{
  constructor(options){
    super(options);
    this.userObserver = new Observer((mutation)=>{
      this.update();
    });
    this.userObserver.observe(rootState.user, {
      recursive: false,
      observed: ['image', 'username'],
      mutations: ['set']
    });
  }
  routeChange(route){
    super.routeChange(route);
    if(this.active){
      this.resetImage();
    }
  }
  resetImage(){
    if(this.profilePicture){
      batch(()=>{
        this.imageInput.value = '';
        if(rootState.user.image){
          this.profilePicture.setAttribute('style', `background-image: url(${APP_URL}/static/${rootState.user.image});`);
        }else{
          this.profilePicture.removeAttribute('style');
        }
      });
    }
  }
  update(){
    batch(()=>{
      if(rootState.user.username){
        this.username.value = rootState.user.username;
      }
    });
    this.resetImage();
  }
  render(...args){
    super.render(...args);

    this.form = this.node.querySelector('form');
    this.username = this.form.querySelector('input[name=username]');
    this.profilePicture = this.node.querySelector('profile-picture');
    this.pictureButton = this.node.querySelector('button');
    this.saveButton = this.form.querySelector('button');
    this.cancelButton = this.form.querySelector('button.inverse');
    this.imageInput = this.node.querySelector('input[type=file]');

    this.imageInput.addEventListener('change', (e)=>{
      if(!e.target.value){
        console.log('no value')
        return;
      }
      if (e.target.files && e.target.files[0]) {
        var reader = new FileReader();
        reader.onload = (evt)=>{
          // console.log(evt.target.result)
          // this.img.setAttribute('src', evt.target.result);
          this.profilePicture.setAttribute('style', `background-image:url(${evt.target.result});`);
        }
        reader.readAsDataURL(e.target.files[0]);
      }else{
        console.log('no files')
      }
    });
    this.form.addEventListener('submit', (evt)=>{
      evt.preventDefault();
      const user = {
        _id: rootState.user._id, // need this for url
        email: rootState.user.email, // need this for unique email action
        username: this.username.value || rootState.user.username || ''
      };
      let j = spinnerService.add(rootState);
      if(this.imageInput.files && this.imageInput.files[0]){
        const formData = new Element.window.FormData();
        formData.append('image', this.imageInput.files[0]);
        userService.updateImage(rootState, formData, (err, res)=>{
          userService.updateUser(user, rootState.token, (e, r)=>{
            Object.assign(rootState.user, r);
            spinnerService.remove(j, rootState);
            routeService.activateRoute(rootState, '');
          });
        });
      }else{
        userService.updateUser(user, rootState.token, (e, r)=>{
          Object.assign(rootState.user, r);
          spinnerService.remove(j, rootState);
          routeService.activateRoute(rootState, '');
        });
      }
    });
    this.cancelButton.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, '');
    });

    this.update();
  }
}

Profile.pattern = /^profile/;
Profile.transclude = true;
Profile.elements = [
  Button,
  Icon,
  ImageAction,
  RouteLink
];