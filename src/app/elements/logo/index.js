import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import Route from '../route';
import Icon from '../icon';
import Button from '../button';
import {rootState} from '../../state';
import {batch} from '../../core';
import {APP_URL} from '../../config';
import {
  routeService
} from '../../services';

export default class Logo extends Route{
  constructor(options){
    super(options);
    this.userObserver = new Observer((mutation)=>{
      this.update();
    });
    this.userObserver.observe(rootState.user, {
      recursive: false,
      observed: ['image'],
      mutations: ['set']
    });
  }
  update(){
    if(this.node){
      batch(()=>{
        if(rootState.user.image){
          this.button.setAttribute('style', `background-image: url(${APP_URL}/static/${rootState.user.image});`);
        }else{
          this.button.removeAttribute('style');
        }
      });
    }
  }
  render(...args){
    super.render(...args);
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      routeService.activateRoute(rootState, 'profile');
    });
    this.update();
  }
}

Logo.elements = [
  Icon,
  Button
];

Logo.pattern = /^\s*$/;
Logo.transclude = true;