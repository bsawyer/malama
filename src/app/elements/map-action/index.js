import Element from '@bsawyer/element';
import Route from '../route';
import {rootState} from '../../state';
import Button from '../button';
import Icon from '../icon';
import {
  routeService
} from '../../services';

export default class MapAction extends Element{
  constructor(options){
    super(options);

  }
  update(){

  }
  render(...args){
    super.render(...args);
    this.update();
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', ()=>{
      rootState.showPanel = false;
    });
  }
}

MapAction.elements = [
  Button,
  Icon
];