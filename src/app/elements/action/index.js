import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import {rootState} from '../../state';
import Route from '../route';
import {batch} from '../../core';
import Button from '../button';
import {Cleanup} from '../../../lib';
import {
  translateService,
  routeService,
  imagesService,
  cleanupService,
  spinnerService
} from '../../services';

const ACTIONS = {
  cleanup: {
    text: 'Clean up',
    click: ()=>{
      routeService.activateRoute(rootState, 'cleanup');
    }
  },
  edit: {
    text: 'Edit',
    click: ()=>{
      routeService.activateRoute(rootState, 'cleanup/:id/edit', rootState.routeParams);
    }
  },
  join: {
    text: 'Join',
    click: ()=>{
      let participant;
      let participants;
      rootState.cleanup.participants.some(p => {
        if(p.userId === rootState.user._id){
          participant = p;
          return true;
        }
      });
      if(participant){
        participant.joined = true;
        participants = rootState.cleanup.participants.slice();
      }else{
        participant = {
         userId: rootState.user._id,
         creator: false,
         joined: true
       };
       participants = rootState.cleanup.participants.slice().concat([participant]);
      }
      let spinnerJob = spinnerService.add(rootState);
      cleanupService.update({participants}, rootState, (err, result)=>{
        spinnerService.remove(spinnerJob, rootState);
        if(err){
          console.log(err);
          return;
        }
        rootState.cleanup = new Cleanup(result);
        rootState.action = 'leave';
      });
    }
  },
  leave: {
    text: 'Leave',
    click: ()=>{
      const participants = rootState.cleanup.participants.slice();
      participants.some(p => {
        if(p.userId === rootState.user._id){
          p.joined = false;
          return true;
        }
      });
      let spinnerJob = spinnerService.add(rootState);
      cleanupService.update({participants}, rootState, (err, result)=>{
        spinnerService.remove(spinnerJob, rootState);
        if(err){
          console.log(err);
          return;
        }
        rootState.cleanup = new Cleanup(result);
        rootState.action = 'join';
      });
    }
  }
};

export default class Action extends Route{
  constructor(options){
    super(options);
    this.showSpinnerObserver = new Observer(()=>{
      this.update();
    });
    this.showSpinnerObserver.observe(rootState, {
      recursive: false,
      observed: ['showSpinner'],
      mutations: ['set']
    });
    this.actionObserver = new Observer(()=>{
      this.update();
    });
    this.actionObserver.observe(rootState, {
      recursive: false,
      observed: ['action'],
      mutations: ['set']
    });
  }
  update(){
    let actionConfig = ACTIONS[rootState.action];
    batch(()=>{
      this.node.classList.toggle('hide', (!!rootState.showSpinner || !actionConfig));
      if(actionConfig){
        this.button.textContent = translateService.getTranslation(actionConfig.text);
      }
    });
  }
  render(...args){
    super.render(...args);
    this.update();
    this.button = this.node.querySelector('button');
    this.button.addEventListener('click', (evt)=>{
      let actionConfig = ACTIONS[rootState.action];
      actionConfig.click && actionConfig.click(evt);
    });
  }
}

Action.elements = [
  Button
];

Action.transclude = true;
Action.pattern = /^\s*$|^cleanup\/:id$/;