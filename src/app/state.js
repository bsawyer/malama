import {
  Root
} from './values';

let state;

if(typeof window !== 'undefined'){
  state = localStorage.getItem('state');
  if(state){
    state = JSON.parse(state);
  }
}

export const rootState = new Root(state || {});