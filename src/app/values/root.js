import {
  User,
  Cleanup
} from '../../lib';

export default class Root{
  constructor({
    registry = {},
    defaultRoute,
    fallbackRoute,
    previousUrl,
    previousRoute,
    previousParams,
    url,
    route,
    routeParams,
    deniedRouteConfig,
    user,
    token,
    modal,
    showModal,
    showSpinner,
    showError,
    showPanel,
    softPromptLocation = true,
    locationPermissionGranted,
    centerCoordinates,
    selectedLocation,
    selectedDate,
    searchLocation,
    searchResults = [],
    searchStatus = 'pending',
    cleanup,
    cleanupCreator,
    action = 'cleanup',
    showImages,
    pendingImages,
    hideImageActions,
    isVisible,
    share
  } = {}){
    this.route = route;
    this.url = url;
    this.registry = registry;
    this.defaultRoute = defaultRoute;
    this.fallbackRoute = fallbackRoute;
    this.previousRoute = previousRoute;
    this.previousParams = previousParams;
    this.routeParams = routeParams;
    this.previousUrl = previousUrl;
    this.user = new User(user); // never null plz
    this.token = token;
    this.modal = modal;
    this.showModal = showModal;
    this.showSpinner = showSpinner;
    this.showError = showError;
    this.showPanel = showPanel;
    this.softPromptLocation = softPromptLocation;
    this.locationPermissionGranted = locationPermissionGranted;
    this.centerCoordinates = centerCoordinates;
    this.selectedLocation = selectedLocation;
    this.selectedDate = selectedDate ? new Date(selectedDate) : null;
    this.searchLocation = searchLocation;
    this.cleanup = cleanup ? new Cleanup(cleanup) : null;
    this.cleanupCreator = cleanupCreator ? new User(cleanupCreator) : null;
    this.action = action;
    this.searchResults = searchResults;
    this.searchStatus = searchStatus;
    this.deniedRouteConfig = deniedRouteConfig;
    this.showImages = showImages;
    this.pendingImages = pendingImages;
    this.hideImageActions = hideImageActions;
    this.isVisible = isVisible;
    this.share = share;
  }
}