const queue = [];

let raf,
    caf;

if(typeof requestAnimationFrame !== 'undefined'){
  raf = requestAnimationFrame;
  caf = cancelAnimationFrame;
}else{
  raf = setTimeout;
  caf = clearTimeout;
}

let id = null,
  requested = false;

function done(){
  id = null;
  requested = false;
  queue.length = 0;
}

function run(){
  for (var i = 0; i < queue.length; i++){
    queue[i].call();
  }
  done();
}

export function cancel(id){
  caf(id);
  done();
}

export function batch(fn){
  queue.push(fn);
  if (!requested){
    id = raf(run);
    requested = true;
  }
  return id;
}