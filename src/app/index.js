import Element from '@bsawyer/element';
import Observer from '@bsawyer/observer';
import workflow from '@bsawyer/workflow';
import {Root} from './elements';
import {rootState} from './state';
import {
  routeService,
  translateService,
  geoService,
  cleanupService,
  spinnerService,
  visibleService,
  userService,
  shareService
} from './services';
import {
  tokenIsValid
} from './policies';
import {
  Root as RootState
} from './values';

Element.window = window;
Element.window.mapboxgl.accessToken = 'pk.eyJ1IjoiYmVuODV0cyIsImEiOiJjaWk0d3N1enAwMWc1dDhtMDIwajMzYXVnIn0.wrDjzEjWdmCjdjY1p35TTw';

const routeConfig = {
  defaultRoute: '',
  fallbackRoute: 'sign-in',
  middleware:{
    auth: authMiddleware,
    token: validAuthMiddleware,
    creator: creatorMiddleware,
    signOut: signOutMiddleware
  },
  registry:{
    '': {
      pattern: '^(\\s*|\\?.*)$',
      middleware: 'auth'
    },
    'search': {
      pattern: '^search$',
      middleware: 'auth'
    },
    'search/location': {
      pattern: '^search/location$',
      middleware: 'auth'
    },
    'cleanup':{
      middleware: 'auth'
    },
    'cleanup/location': {
      pattern: '^cleanup/location',
      middleware: 'auth'
    },
    'cleanup/:id':{
      pattern: '^cleanup/(?!location)([0-9]|[A-z])+$',
      middleware: 'auth'
    },
    'cleanup/:id/location': {
      pattern: '^cleanup/([0-9]|[A-z])+/location$',
      middleware: 'auth'
    },
    'cleanup/:id/edit':{
      pattern: '^cleanup/([0-9]|[A-z])+/edit$',
      middleware: 'auth'
    },
    'profile':{
      pattern: '^profile',
      middleware: 'auth'
    },
    'sign-in':{
      pattern: '^sign-in',
      middleware: 'token'
    },
    'sign-up':{
      pattern: '^sign-up',
      middleware: 'token'
    },
    'sign-out': {
      pattern: '^sign-out',
      middleware: 'signOut',
      disableRestore: true
    },
    'password-reset':{
      pattern: '^password-reset',
      middleware: 'token'
    },
    'reset':{
      pattern: '^reset',
      middleware: 'token'
    }
  }
};

const workfowConfig = {
  concurrent: false,
  actions: [
    visibleService.init,
    spinnerService.init,
    translateService.init,
    userService.init,
    geoService.init,
    shareService.init
  ]
};

//return true if we cant activate
function authMiddleware(state){
  return !state.token;
}

function validAuthMiddleware(state){
  return tokenIsValid(state.token);
}

// cant have this without async route resolves
function creatorMiddleware(state){
  return !(state.cleanup && state.user && state.cleanup.participants.some(p => p.userId === state.user._id && p.creator));
}

function signOutMiddleware(state){
  const root = new RootState({
    registry: state.registry,
    defaultRoute: state.defaultRoute,
    fallbackRoute: state.fallbackRoute,
    previousUrl: state.previousUrl,
    previousRoute: state.previousRoute,
    previousParams: state.previousParams,
    url: state.url,
    route: state.route,
    routeParams: state.routeParams
  });
  workflow(workfowConfig, root, (err)=>{
    Object.assign(state, root);
  });
  return true;
}

workflow(workfowConfig, rootState, (err)=>{
  console.log('cool');
});

routeService.init(rootState, routeConfig);

let setStorageTimeout;
let observer = new Observer(()=>{
  if(setStorageTimeout){
    return;
  }
  // just so we dont block
  setStorageTimeout = setTimeout(()=>{
    localStorage.setItem('state', JSON.stringify(rootState));
    setStorageTimeout = null;
  });
});

observer.observe(rootState, {recursive: true});

const root = new Root();
root.render(document.querySelector('root'), true);

window.addEventListener('load', e => {
  navigator.serviceWorker.register('/service-worker.js');
});