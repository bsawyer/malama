import {URL} from '../config';
import {User} from '../../lib';

function createToken(user, next){
  return fetch(`${URL}/api/auth`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(user)
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function createPasswordReset(user, next){
  // endpoint doesnt return payload
  return fetch(`${URL}/api/user/password`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(user)
  })
  .then((response) => {
    if(response.ok){
      return next(null, user);
    }
    return response.json()
      .then(body => {
        return next(body);
      });
  })
  .catch(err => next(err));
}

function createUser(user, next){
  // endpoint doesnt return payload
  return fetch(`${URL}/api/user`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(user)
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function updateUser(user, token, next){
  return fetch(`${URL}/api/user/${user._id}`, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': token
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(user)
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function updateImage(state, formData, next){
  return fetch(`${URL}/api/user/${state.user._id}/image`, {
    method: 'PUT',
    headers: new Headers({
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default',
    body: formData
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function getUser(id, token, next){
  return fetch(`${URL}/api/user/${id}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': token
    }),
    mode: 'cors',
    cache: 'default'
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function getUserAndAssign(state, next){
  getUser(state.user._id, state.token, (e, user)=>{
    if(e){
      console.log(e);
      next(e);
      return;
    }
    Object.assign(state.user, new User(user));
    next();
  });
}

function init(state, next){
  if(state.token && state.user._id){
    getUserAndAssign(state, next);
  }else{
    next();
  }
}

export default {
  createToken,
  createPasswordReset,
  createUser,
  updateUser,
  updateImage,
  getUser,
  getUserAndAssign,
  init
}