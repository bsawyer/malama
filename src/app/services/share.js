import {default as modalService} from './modal';

function init(state){
  state.share = null;
}

function share(state, cleanup, next){
  if(navigator.share){
    navigator.share({
      title: 'Malama - Cleanup',
      text: 'Show your support by celebrating community cleanups',
      url: `https://malama.cc/cleanup/${cleanup._id}`
    })
    .then(() => next())
    .catch(next);
  }else{
    state.share = cleanup;
    modalService.share(state, ()=>{
      state.share = null;
      next();
    });
  }
}

export default {
  init,
  share
}