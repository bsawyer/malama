// 0 - 11 am, 12 - 23 pm
// 0 === 12 am
// 1 === 1 am
// 11 === 11 am
// 12 === 12 pm
// 13 === 1 pm
// 23 === 11 pm

function parse(date){
  let hour = date.getHours();
  let dd = 'am';
  if(hour >= 12){
    hour = hour - 12;
    dd = 'pm';
  }
  if(hour === 0){
    hour = 12;
  }
  return {
    month: date.getMonth() + 1,
    day: date.getDate(),
    year: parseFloat(date.getFullYear().toString().substr(-2)),
    hour,
    minute: date.getMinutes(),
    second: date.getSeconds(),
    dd,
    ms: date.getTime(),
    offset: date.getTimezoneOffset()
  }
}

function create(parsed){
  if(parsed.ms){
    return new Date(parsed.ms);
  }
  const date = new Date();
  date.setMonth(parsed.month - 1);
  date.setDate(parsed.day);
  date.setFullYear('20' + parsed.year);
  date.setHours(parsed.dd.toLowerCase() === 'am' ? (parsed.hour === 12 ? 0 : parsed.hour) : (parsed.hour === 12 ? 12 : parsed.hour + 12));
  date.setMinutes(parsed.minute);
  date.setSeconds(parsed.second);
  return date; //in the users timezone
}

export default{
  parse,
  create
}