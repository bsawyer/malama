import {default as userService} from './user';

function init(state){
  document.addEventListener('visibilitychange', ()=>{
    const isVisible = document.visibilityState === 'visible';
    if(isVisible && !state.isVisible && state.token && state.user._id){
      userService.getUserAndAssign(state, ()=>{});
    }
    state.isVisible = isVisible;
  }, false);
}

export default {
  init
}