import {default as translations} from '../translations';
import {rootState} from '../state';
import {batch} from '../core';
import {SUPPORTED_LANGUAGES} from '../config';

function getTranslation(val){
  return translations[val] ? translations[val][rootState.user.preferences.language] || val : val;
}

function translateElements(node, config){
  Object.keys(config).forEach((selector)=>{
    Array.prototype.forEach.call(node.querySelectorAll(selector), (n)=>{
      if(config[selector].text){
        let val = n.textContent;
        let translated = getTranslation(val);
        if(translated !== val){
          batch(()=>{
            n.textContent = translated;
          });
        }
      }
      if(config[selector].attr){
        config[selector].attr.forEach((attr)=>{
          let v = n.textContent;
          let t = getTranslation(n.getAttribute(attr));
          if(t !== v){
            batch(()=>{
              n.setAttribute(attr, t);
            });
          }
        });
      }
    });
  });
}

function init(state){
  const lang = navigator.languages.reduce(
    (prev, l) => SUPPORTED_LANGUAGES.indexOf(l.split('-')[0]) !== -1 ? l.split('-')[0] : prev, 'en'
  );
  // should this update the user or just let api look at accept header?
  // also when we do get user, do we override this?
  // also need to get the users locale country
  state.user.preferences.language = lang;
}

export default {
  getTranslation,
  translateElements,
  init
}