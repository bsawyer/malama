const minimumDelay = 1200;
let pending = [];
let jobID = 0;
let timeout;

function init(state){
  state.showSpinner = false;
  if(timeout){
    clearTimeout(timeout);
    timeout = null;
  }
  jobID = 0;
  pending = [];
}

function add(state){
  jobID++;
  pending.push(jobID);
  toggleShowSpinner(state);
  return jobID;
}

function remove(job, state){
  if(!job) return;
  const i = pending.indexOf(job);
  if(i !== -1){
    pending.splice(i, 1);
    toggleShowSpinner(state);
  }
}

function toggleShowSpinner(state){
  if(pending.length){
    state.showSpinner = true;
    if(!timeout){
      timeout = setTimeout(()=>{
        timeout = null;
        toggleShowSpinner(state);
      }, minimumDelay);
    }
  }else{
    if(!timeout){
      state.showSpinner = false;
    }
  }
}

export default {
  init,
  add,
  remove
}