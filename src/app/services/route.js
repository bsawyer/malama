let middleware = {};

// uses url to find route key
function getRegistryRoute(state, url){
  let d = null;
  Object.keys(state.registry).some(k => {
    if(state.registry[k].pattern){
      if(url.match(new RegExp(state.registry[k].pattern)) !== null){
        d = k;
        return true;
      }
    }else{
      if(k === url){
        d = k;
        return true;
      }
    }
  });
  return d;
}

function getDefaultRoute(state){
  return state.defaultRoute;
}

function getActiveRouteParam(state, param){
  return state.url.split('/')[state.route.split('/').indexOf(`:${param}`)];
}

function getActiveQueryParams(state){
  const params = {};
  let queryIndex = state.url.indexOf('?');
  if(queryIndex === -1 || queryIndex >= state.url.length - 1){
    return params;
  }
  state.url
    .substring(queryIndex+1)
    .split('&')
    .forEach(function paramParseMap(pair){
        if(!pair) return;
        pair = pair.split('=');
        params[pair[0]] = pair.length === 1 ? null : pair[1];
    });
  return params;
}

function register(state, route, config){
  state.registry[route] = config;
}

// this is more of a policy
function isRoute(state, url){
  return getRegistryRoute(state, url) !== null;
}

// uses route key to lookup
function canActivate(state, route){
  // const registryRoute = getRegistryRoute(state, route);
  const registryConfig = state.registry[route];
  if(!registryConfig) return false;
  if(registryConfig.middleware){
    // could be an array or string
    let routeMiddleware;
    if(Array.isArray(registryConfig.middleware)){
      routeMiddleware = registryConfig.middleware.map(k => middleware[k]);
    }else{
      routeMiddleware = middleware[registryConfig.middleware];
    }
    // does middleware say we cant?
    // middleware must return true if it can't activate
    return routeMiddleware.call ?
    !routeMiddleware(state, route, registryConfig) :
    !routeMiddleware.some(m => m(state, route, registryConfig));
  }
  return true;
}

function getRouteParams(route, url){
  const pathParams = {};
  route.split('/').forEach((part,i)=>{
    if(part.indexOf(':') === 0){
      pathParams[part.substring(1)] = url.split('/')[i];
    }
  });
  return pathParams;
}

function assignRoute(state, url, params, replace){
  const route = getRegistryRoute(state, url);

  if(state.url === url){
    return;
  }

  // extract more params from route
  const pathParams = getRouteParams(route, url);

  params = Object.assign({}, params, pathParams);

  if(!replace){
    state.previousUrl = state.url;
    state.previousParams = state.routeParams;
    state.previousRoute = state.route;
  }

  state.url = url;
  state.routeParams = params;
  state.route = route; //this is what is observed so update this last!

  // state.deniedRouteConfig = null; // here?
}

function formatUrl(route, params = {}){
  Object.keys(params).forEach((key)=>{
    route = route.replace(`:${key}`, params[key]);
  });
  return route;
}

// uses an actual route name/key and params to activate
function activateRoute(state, route, params, replace){
  if(!canActivate(state, route)){
    // console.error(`Failed to resolve '${route}'`);
    route = state.fallbackRoute;
    params = {};
    replace = true;
  }
  const url = formatUrl(route, params);
  const registryRoute = getRegistryRoute(state, url);
  window.history[replace ? 'replaceState' : 'pushState']({registryRoute}, null, `/${url}`);
  assignRoute(state, url, params, replace);
}

function canRestoreRoute(state, route){
  const registryConfig = state.registry[route];
  return !registryConfig.disableRestore;
}

// only run on popstate with a url
function updateRoute(state, url, params){
  if(isRoute(state, url)){
    const route = getRegistryRoute(state, url);
    if(canActivate(state, route)){
      assignRoute(state, url, params); // browser navigate, url is already set
      return;
    }
    // need to keep track of a denied route
    // so that at a later time we may want to restore it
    // we should also clear the denied route if this route is activated
    if(canRestoreRoute(state, route)){
      state.deniedRouteConfig = {
        route,
        url,
        params: getRouteParams(route, url)
      };
    }
  }

  // restore route
  const r = state.route;
  const p = state.routeParams;
  if(r && canActivate(state, r)){
    activateRoute(state, r, p, true);
    return;
  }

  // restore default
  const defaultRoute = getDefaultRoute(state);
  if(canActivate(state, defaultRoute)){
    activateRoute(state, defaultRoute, {}, true); //should have defaultParams?
    return;
  }

  // restore fallback
  const fallbackRoute = state.fallbackRoute; // must be able to always activate default route
  activateRoute(state, fallbackRoute, {}, true);
}

function onPopstate(state){
  const pathname = location.pathname.replace(/^\/|\/$/g, '');
  const search = decodeURI(location.search);
  const params = {};
  if(search && search.length){
    search.substring(1).split('&').forEach(str => {
      const kv = str.split('=');
      if(kv[0].length){
        params[kv[0]] = kv[1];
      }
    });
  }
  updateRoute(state, pathname + search, params);
}

function init(state, config){
  state.defaultRoute = config.defaultRoute;
  state.fallbackRoute = config.fallbackRoute;
  Object.assign(middleware, config.middleware);
  Object.keys(config.registry)
    .forEach((route)=>{
      register(state, route, config.registry[route]);
    });
  // only fired from browser client click
  // need fix for popstate onload for firefox
  window.addEventListener('popstate', () => {
    onPopstate(state);
    // window.history.pushState
    // window.history.replaceState

    // check if this is a valid path
    // if it is write it to root state
    // if not, go back
    // if cant go back, go to default
  });

  onPopstate(state);
}

export default {
  register,
  isRoute,
  canActivate,
  getDefaultRoute,
  getRegistryRoute,
  getActiveRouteParam,
  getActiveQueryParams,
  activateRoute,
  init
}