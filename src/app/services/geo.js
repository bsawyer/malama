import {default as userService} from './user';
import {Location} from '../../lib';
import {tokenIsValid} from '../policies';
import {default as spinnerService} from './spinner';

const API_KEY = 'pk.eyJ1IjoiYmVuODV0cyIsImEiOiJjaWk0d3N1enAwMWc1dDhtMDIwajMzYXVnIn0.wrDjzEjWdmCjdjY1p35TTw';
const SEARCH_URL = 'https://api.mapbox.com/geocoding/v5/mapbox.places';

const mapState = {};

function centerMap(state){
  if(mapState.map && state.centerCoordinates){
    mapState.map.flyTo({
      center: state.centerCoordinates,
      zoom: state.user.preferences.zoom
    });
  }
}

function addMarkertoMap(){
  if(mapState.userMarker && mapState.map){
    mapState.userMarker.addTo(mapState.map);
  }
}

function registerMap(map){
  mapState.map = map;
  addMarkertoMap();
}

function registerUserMarker(userMarker){
  mapState.userMarker = userMarker;
  addMarkertoMap();
}

function updateUserLocation(state, position){
  const location = new Location({
    coordinates: [position.coords.longitude, position.coords.latitude]
  });
  state.user.location = location;
  state.locationPermissionGranted = true;
  if(tokenIsValid(state.token)){
    userService.updateUser({location, _id: state.user._id}, state.token, (err)=>{
      if(err){
        console.log(err);
      }
    });
  }
}

function getLocationAndWatch(state, next){
  let debounceLocationUpdateTimeout;
  let lastPosition;

  getLocation(state, next);

  state.watchId = navigator.geolocation.watchPosition(
   (position)=>{
     lastPosition = position;
     if(!debounceLocationUpdateTimeout){
       debounceLocationUpdateTimeout = setTimeout(()=>{
         updateUserLocation(state, lastPosition);
         debounceLocationUpdateTimeout = null;
       }, 500);
     }
   },
   (err)=>{
     console.log(err);
     // user denied it?
     if(err.code === 1){
      state.locationPermissionGranted = false;
     }
   },{
     enableHighAccuracy: true,
     maximumAge: 30000
   }
  );
}

function getLocation(state, next){
  navigator.geolocation.getCurrentPosition((position)=>{
    updateUserLocation(state, position);
    next(null);
  }, next, {
    timeout: 6000
  });
}

function init(state){
  if('geolocation' in navigator){
    // first init with no previous state in storage
    if(state.softPromptLocation){
      return;
    }
    let j = spinnerService.add(state);
    getLocationAndWatch(state, (err)=>{
      spinnerService.remove(j, state);
      state.softPromptLocation = false;
      if(err){
        // maybe set something on state to so we can ask user to fix permission
        if(err.code === 1){
          state.locationPermissionGranted = false;
        }
        return;
      }
    });
    return;
  }
  state.softPromptLocation = false;
}

function search(query, next){
  return fetch(`${SEARCH_URL}/${encodeURI(query)}.json?access_token=${API_KEY}&autocomplete=true`, {
    method: 'GET',
    headers: new Headers(),
    mode: 'cors',
    cache: 'default'
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

export default {
  init,
  getLocation,
  getLocationAndWatch,
  registerMap,
  registerUserMarker,
  centerMap,
  search
}