import {default as geoService} from './geo';
import {default as spinnerService} from './spinner';

function softPromptLocation(state){
  state.modal = {
    content: `Malama works by finding cleanups near you. Would you like to enable location services?`,
    button: `Enable`,
    deny: `Skip`,
    onDismiss: (result)=>{
      if(result){
        // user agreed to soft prompt
        let j = spinnerService.add(state);
        geoService.getLocationAndWatch(state, (err)=>{
          spinnerService.remove(j, state);
          if(err){
            //error trying to get location or watch
            console.log(err);
            if(err.code === 1){
              state.locationPermissionGranted = false;
            }
            return;
          }
          state.centerCoordinates = state.user.location.coordinates;
        });
        state.softPromptLocation = false;
      }
    }
  };
  state.showModal = true;
}

function passwordResetSuccess(state, user){
  state.modal = {
    content: `A reset password link was sent to '${user.email}'`,
    button: 'OK'
  };
  state.showModal = true;
}

function share(state, next){
  let content = `https://malama.cc/cleanup/${state.share._id}`;
  state.modal = {
    content,
    button: 'Copy',
    deny: 'Back',
    onDismiss: (result)=>{
      if(result){
        // copy link to clipboard;
        let i = document.createElement('input');
        i.value = content;
        document.body.appendChild(i);
        i.select();
        document.execCommand('copy');
        i.remove();
      }
      next();
    }
  };
  state.showModal = true;
}

export default {
  softPromptLocation,
  passwordResetSuccess,
  share
}