import {URL} from '../config';

function create(cleanup, state, next){
  return fetch(`${URL}/api/cleanup`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(cleanup)
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function update(cleanup, state, next){
  return fetch(`${URL}/api/cleanup/${state.cleanup._id}`, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(cleanup)
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

function get(id, state, next){
  return fetch(`${URL}/api/cleanup/${id}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default'
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => {
    next(err)
  });
}

function search(state, next){
  let params = `?status=${state.searchStatus}`;
  if(state.searchLocation){
    params += `&coordinates=${encodeURI(state.searchLocation.geometry.coordinates.join(','))}`;
  }
  return fetch(`${URL}/api/cleanup${params}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default'
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => {
    next(err)
  });
}

export default {
  create,
  update,
  get,
  search
}