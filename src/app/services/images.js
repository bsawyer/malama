import workflow from '@bsawyer/workflow';

import {
  userIsCleanupCreator,
  cleanupHasMaxImages
} from '../../lib';

import {URL} from '../config';

const imagesState = {
  input: null,
  fileMap: {}
};

function init(state, input){
  state.showImages = false;
  state.pendingImages = [];
  imagesState.input = input;
  imagesState.input.addEventListener('change', (e)=>{
    if(!e.target.value){
      return;
    }
    if(e.target.files && e.target.files.length){
      Array.prototype.slice.call(e.target.files).forEach((file)=>{
        // if(file.size > 5000000){
        //   alert('File is too big!');
        //   return;
        // }
        var reader = new FileReader();
        reader.onload = (evt)=>{
          // imagesState.fileMap[evt.target.result]  =
          const fileMapKey = new Date().getTime() + evt.target.result.slice(0, 5);
          imagesState.fileMap[fileMapKey] = {dataURL: evt.target.result, file, fileMapKey};
          state.pendingImages.push({fileMapKey});
          state.pendingImages = state.pendingImages; // eslint-disable-line no-self-assign
        };
        reader.readAsDataURL(file);
      });
    }
  });
}

//pendingImage is either the filename string or the mapped file object
function remove(state, pendingImage){
  if(pendingImage.fileMapKey){
    delete imagesState.fileMap[pendingImage.fileMapKey];
    let index = null;
    state.pendingImages.some((p,i)=> {
      if(p.fileMapKey && p.fileMapKey === pendingImage.fileMapKey){
        index = i;
        return true;
      }
    });
    if(index !== null){
      state.pendingImages.splice(index, 1);
    }
  }else{
    state.pendingImages.splice(state.pendingImages.indexOf(pendingImage), 1);
  }
  state.pendingImages = state.pendingImages; // eslint-disable-line no-self-assign
}

function show(state, hideActions){
  state.showImages = true;
  state.hideImageActions = !!hideActions;
}

function hide(state){
  state.showImages = false;
}

function clearPending(state){
  state.pendingImages = [];
}

function setPending(state){
  state.pendingImages = state.cleanup && state.cleanup.images ? state.cleanup.images.slice() : [];
}

function add(state){
  imagesState.input.value = '';
  imagesState.input.click();
}

function clearFiles(){
  imagesState.fileMap = {};
}

function getFile(key){
  return imagesState.fileMap[key];
}

function updateCleanup(state, cleanup, next){
  if(!state.pendingImages || !state.pendingImages.length){
    return next(null, cleanup);
  }
  const actions = state
    .pendingImages.filter(p => p.fileMapKey)
    .map((p)=>{
      const file = getFile(p.fileMapKey).file;
      const formData = new window.FormData();
      formData.append('image', file);
      return (ctx, nxt)=>{
        updateCleanupImage(state, cleanup._id, formData, (err, response)=>{
          ctx.cleanup = response;
          nxt(err);
        });
      };
    });
  if(!actions.length){
    return next(null, cleanup);
  }
  const context = {
    cleanup
  };
  workflow({concurrent: true, actions}, context, (err)=>{
    next(err, context.cleanup);
  });
}

function updateCleanupImage(state, cleanupId, formData, next){
  return fetch(`${URL}/api/cleanup/${cleanupId}/image`, {
    method: 'PUT',
    headers: new Headers({
      'x-access-token': state.token
    }),
    mode: 'cors',
    cache: 'default',
    body: formData
  })
  .then(response => {
    return response.json()
      .then(body => {
        return {
          body,
          response
        }
      });
  })
  .then(({body, response}) => {
    if(response.ok){
      return next(null, body);
    }
    return next(body);
  })
  .catch(err => next(err));
}

export default {
  init,
  show,
  hide,
  add,
  remove,
  clearPending,
  setPending,
  clearFiles,
  getFile,
  updateCleanup
}