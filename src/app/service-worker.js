const cacheName = 'pwa-malama-v3';
const staticAssets = [
  './',
  './index.html',
  './index.js',
  './index.css'
];

// Any change in this file will trigger a new install event to allow you to update the app.
self.addEventListener('install', event => {
  console.log('install');
  // caches.open(cacheName).then((cache)=>{
  //   // The addAll operation will fail if any of the assets fail to be fetched, so be careful with the paths.
  //   cache.addAll(staticAssets).then(()=>{
  //     console.log('cached')
  //   })
  // });
});

self.addEventListener('fetch', event => {
  // const req = event.request;
  // event.respondWith(cacheFirst(req));
});


function cacheFirst(req) {
  return caches.open(cacheName)
  .then((cache)=>{
    return cache.match(req);
  })
  .then((cachedResponse)=>{
    return cachedResponse || fetch(req);
  });
}