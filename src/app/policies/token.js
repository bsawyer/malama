export function tokenIsValid(t){
  if(!t) return false;
  const encoded = t.split('.')[1];
  let decoded;
  if(encoded){
    try {
      decoded = JSON.parse(atob(encoded));
    } catch (e) {
      return false;
    }
  }else{
    return false;
  }
  return new Date(new Date().toISOString()) < new Date(decoded.exp * 1000);
}