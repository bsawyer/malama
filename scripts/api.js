const workflow = require('@bsawyer/workflow');
const api = require('./actions/api');
const apiMkdirp = require('./actions/api-mkdirp');
const apiDev = require('./actions/api-dev');

const path = require('path');
const fs = require('fs');
const config = require('./config');

const build = {
  concurrent: false,
  actions: [
    apiMkdirp,
    api,
    apiDev
  ]
};

let stop = workflow(
  build,
  {},
  err => {
    if(err){
      console.log('\x1b[31m%s\x1b[0m', err);
      stop();
    }
  }
);

if(process.argv[2] && process.argv[2] === '-w'){
  // crap i have to watch lib dir too : (
  fs.watch(path.resolve(config.SRC_PATH, config.API_PATH),
    {recursive: true},
    ()=>{
      console.log('file change');
      stop();
      stop = workflow(
        build,
        {},
        err => {
          if(err){
            console.log('\x1b[31m%s\x1b[0m', err);
            stop();
          }
        }
      );
    });
}