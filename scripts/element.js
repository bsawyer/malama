const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const config = require('./config');

const files = [
  'index.js',
  'index.spec.js',
  'index.styl',
  'template.html'
];

const elementPath = path.resolve(config.SRC_PATH, config.APP_PATH, config.ELEMENT_PATH);

const elements = process.argv.slice(2)

elements.map(seedElement);

requireStyle(elements);

function seedElement(element){
  const dir = path.resolve(elementPath, element);
  if(!fs.existsSync(dir)){
    mkdirp.sync(dir);
    files.forEach(f => fs.writeFileSync(path.resolve(dir, f),''));
    console.log(`created ${element}`);
  }else{
    console.log(`${element} already exists`);
  }
}

function requireStyle(elms){
  if(!elms.length) return;
  const p = path.resolve(elementPath, './index.styl');
  let style = fs.readFileSync(p);
  elms.forEach((name)=>{
    style += `@require './${name}'\n`;
  });
  fs.writeFileSync(p, style);
}