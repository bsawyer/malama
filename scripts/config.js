const path = require('path');

const SRC_PATH = path.resolve(__dirname, '../src');
const BUILD_PATH = path.resolve(__dirname, '../build');

const API_PATH = './api';
const APP_PATH = './app';
const WWW_PATH = './www';
const LIB_PATH = './lib';

const APP_STATIC_PATH = './static';

const ELEMENT_PATH = './elements';

const REPLACE_MAP = {
  local: {
    REPLACE_API_URL: '',
    REPLACE_APP_URL: ''
  },
  production: {
    REPLACE_API_URL: '',
    REPLACE_APP_URL: ''
  }
};

module.exports = {
  SRC_PATH,
  BUILD_PATH,
  API_PATH,
  APP_PATH,
  WWW_PATH,
  LIB_PATH,
  APP_STATIC_PATH,
  ELEMENT_PATH,
  REPLACE_MAP
};