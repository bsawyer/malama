const workflow = require('@bsawyer/workflow');
const appMkdirp = require('./actions/app-mkdirp');
const appStylus = require('./actions/app-stylus');
const appPug = require('./actions/app-pug');
const appJs = require('./actions/app-js');
const api = require('./actions/api');
const apiMkdirp = require('./actions/api-mkdirp');
const appCopyStatic = require('./actions/app-copy-static');
const wwwCopy = require('./actions/www-copy');

const path = require('path');
const fs = require('fs');
const config = require('./config');

const build = {
  concurrent: false,
  actions: [
    appMkdirp,
    apiMkdirp,
    appCopyStatic,
    wwwCopy,
    {
      concurrent: true,
      actions: [
        appStylus,
        appPug,
        appJs,
        api
      ]
    }
  ]
};

let stop = workflow(
  build,
  {},
  err => {
    if(err){
      console.log('\x1b[31m%s\x1b[0m', err);
      stop();
    }
  }
);

if(process.argv[2] && process.argv[2] === '-w'){
  // crap i have to watch lib dir too : (
  fs.watch(path.resolve(config.SRC_PATH),
    {recursive: true},
    ()=>{
      console.log('file change');
      stop && stop();
      stop = workflow(
        build,
        {},
        err => {
          if(err){
            console.log('\x1b[31m%s\x1b[0m', err);
            stop();
          }
        }
      );
    });
}