const mkdirp = require('mkdirp');
const createFilter = require('rollup-pluginutils').createFilter;
const fs = require('fs');
const path = require('path');

function createMkdirpAction(dir){
  return function(ctx, next){
    console.log(`starting mkdirp '${dir}'...`);
    mkdirp(dir, (err)=>{
      if (err) return next(err);
      console.log(`mkdirp '${dir}' done`);
      next();
    });
  }
}

function elementPlugin(options = {}){
  const filter = createFilter( options.include, options.exclude );
  return {
    transform ( code, id ) {
      // if `options.include` is omitted or has zero length, filter
      // will return `true` by default. Otherwise, an ID must match
      // one or more of the minimatch patterns, and must not match
      // any of the `options.exclude` patterns.
      if ( !filter( id ) ) return;
      // proceed with the transformation...
      const elementClassName = code.match(/class [A-z]*/)[0].replace('class ', '');
      console.log(`adding template to '${elementClassName}' at '${id}'`);
      try {
        const templateSrc = fs.readFileSync(path.resolve(path.dirname(id), 'template.html'));
        code = code + elementClassName + '.templateString = `' + templateSrc + '`;'
      } catch (e) {
        //  console.log(e)
      }
      return code;
    }
  };
}

module.exports = {
  createMkdirpAction,
  elementPlugin
};