const path = require('path');
const fs = require('fs');
const config = require('../config');
const mkdirp = require('mkdirp');
var ncp = require('ncp').ncp;

// copies src/app/static to /build/app
// copies service worker to /build/app
module.exports = function(ctx, next){
  ncp(path.resolve(config.SRC_PATH, config.APP_PATH, config.APP_STATIC_PATH), path.resolve(config.BUILD_PATH, config.APP_PATH), function (e) {
   if (e) {
     return next(e);
   }
   fs.copyFile(path.resolve(config.SRC_PATH, config.APP_PATH, 'service-worker.js'), path.resolve(config.BUILD_PATH, config.APP_PATH, 'service-worker.js'), function (er) {
    if (er) {
      return next(er);
    }
    next();
   });
  });
};