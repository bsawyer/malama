const path = require('path');
const fs = require('fs');
const config = require('../config');
const mkdirp = require('mkdirp');
var ncp = require('ncp').ncp;

// copies src/www to /build/www
module.exports = function(ctx, next){
  ncp(path.resolve(config.SRC_PATH, config.WWW_PATH), path.resolve(config.BUILD_PATH, config.WWW_PATH), function (e) {
   if (e) {
     return next(e);
   }
   next();
  });
};