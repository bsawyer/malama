const fs = require('fs');
const path = require('path');
const config = require('../config');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const cjs = require('rollup-plugin-commonjs');
const json = require('rollup-plugin-json');
const resolve = require('rollup-plugin-node-resolve');
const replace = require('rollup-plugin-replace');
const createFilter = require('rollup-pluginutils').createFilter;
const elementPlugin = require('../util').elementPlugin;

// function elementPlugin(options = {}){
//   const filter = createFilter( options.include, options.exclude );
//   return {
//     transform ( code, id ) {
//       // if `options.include` is omitted or has zero length, filter
//       // will return `true` by default. Otherwise, an ID must match
//       // one or more of the minimatch patterns, and must not match
//       // any of the `options.exclude` patterns.
//       if ( !filter( id ) ) return;
//       // proceed with the transformation...
//       const elementClassName = code.match(/class [A-z]*/)[0].replace('class ', '');
//       console.log(`adding template to '${elementClassName}' at '${id}'`);
//       try {
//         const templateSrc = fs.readFileSync(path.resolve(path.dirname(id), 'template.html'));
//         code = code + elementClassName + '.templateString = `' + templateSrc + '`;'
//       } catch (e) {
//         // console.log(e)
//       }
//       return code;
//     }
//   };
// }

module.exports = function appJs(ctx, next){
  console.log(`starting app js...`);
  const replaceConfig = Object.assign({}, config.REPLACE_MAP[process.env.BUILD_ENV || 'local']);
  rollup.rollup({
    onwarn: (err)=>{
      if(err) return console.log('\x1b[33m%s\x1b[0m', err);
    },
    input: path.resolve(config.SRC_PATH, config.APP_PATH, 'index.js'),
    plugins: [
      replace(replaceConfig),
      resolve(),
      cjs(),
      elementPlugin({include: ['src/app/elements/**'], exclude: ['src/app/elements/index.js']}),
      json(),
      babel()
    ]
  })
  .then((bundle)=>{
    return bundle.generate({
      format: 'iife',
      file: 'index.js',
      name: 'malama'
    });
  })
  .then(({code, map})=>{
    fs.writeFileSync(path.resolve(config.BUILD_PATH, config.APP_PATH, 'index.js'), code);
    console.log(`app js done`);
    next();
  })
  .catch((err)=>{
    next(err);
  });
};