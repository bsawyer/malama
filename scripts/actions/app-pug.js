const pug = require('pug');
const fs = require('fs');
const path = require('path');
const config = require('../config');

const replaceConfig = Object.assign({}, config.REPLACE_MAP[process.env.BUILD_ENV || 'local']);

module.exports = function appPug(ctx, next){
  console.log(`app pug action starting...`);
  const compileFunction = pug.compileFile(path.resolve(config.SRC_PATH, config.APP_PATH, 'index.pug'), {pretty: true});

  const icons = JSON.parse(fs.readFileSync(path.resolve(config.SRC_PATH, config.APP_PATH, 'icons.json')));

  fs.writeFile(path.resolve(config.BUILD_PATH, config.APP_PATH, 'index.html'), compileFunction({
    lang: 'en',
    url: replaceConfig.REPLACE_APP_URL, //config.APP_URL
    icons
  }), (e)=>{
    if(e) return next(e);
    console.log(`app pug action done`);
    next();
  });

};