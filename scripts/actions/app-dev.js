const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;
const config = require('../config');

module.exports = function(ctx){
  const app = express();
  console.log('starting app dev');

  app.use(express.static(path.resolve(config.BUILD_PATH, config.APP_PATH)));

  app.get('*', function (request, response) {
    response.sendFile(path.resolve(config.BUILD_PATH, config.APP_PATH, 'index.html'));
  });

  app.listen(port);
};