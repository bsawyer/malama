const fs = require('fs');
const path = require('path');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');
const replace = require('rollup-plugin-replace');
const config = require('../config');
const elementPlugin = require('../util').elementPlugin;

module.exports = function(ctx, next){
  console.log(`starting api...`);
  const replaceConfig = Object.assign({}, config.REPLACE_MAP[process.env.BUILD_ENV || 'local']);
  rollup.rollup({
    onwarn: (err)=>{
      if(err) return console.log('\x1b[33m%s\x1b[0m', err);
    },
    input: path.resolve(config.SRC_PATH, config.API_PATH, 'index.js'),
    plugins: [
      replace(replaceConfig),
      resolve({
        only: [/^\.{0,2}\//, /^@bsawyer\/.*$/ ]
      }),
      elementPlugin({include: ['src/app/elements/**'], exclude: ['src/app/elements/index.js']}),
      babel()
    ]
  })
  .then((bundle)=>{
    return bundle.generate({
      format: 'cjs',
      file: 'index.js'
    });
  })
  .then(({code, map})=>{
    fs.writeFileSync(path.resolve(config.BUILD_PATH, config.API_PATH, 'index.js'), code);
    console.log(`api done`);
    next();
  })
  .catch((err)=>{
    next(err);
  });
};