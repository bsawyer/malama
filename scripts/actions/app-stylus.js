const fs = require('fs');
const path = require('path');
const stylus = require('stylus');
const config = require('../config');

const replaceConfig = Object.assign({}, config.REPLACE_MAP[process.env.BUILD_ENV || 'local']);

module.exports = function appStylus(ctx, next){
  console.log(`app stylus action starting...`);
  fs.readFile(path.resolve(config.SRC_PATH, config.APP_PATH, 'index.styl'), 'utf8', (error, index)=>{
    if(error) return next(error);
    stylus(index)
      .define('$url', replaceConfig.REPLACE_APP_URL)
      .set('paths', [
        path.resolve(config.SRC_PATH, config.APP_PATH)
      ])
      .render((err, css)=>{
        if(err) {
          return next(err);
        }
        fs.writeFile(path.resolve(config.BUILD_PATH, config.APP_PATH, 'index.css'), css, (e)=>{
          if(e) return next(e);
          console.log(`app stylus action done`);
          next(null, css);
        });
      });
  });
};