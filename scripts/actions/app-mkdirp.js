const config = require('../config');
const path = require('path');
const createMkdirpAction = require('../util').createMkdirpAction;

module.exports = createMkdirpAction(path.resolve(config.BUILD_PATH, config.APP_PATH));