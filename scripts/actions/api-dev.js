const path = require('path');
const fs = require('fs');
const {spawn} = require('child_process');
const config = require('../config');

module.exports = function(ctx, next){
  const filePath = path.resolve(config.BUILD_PATH, config.API_PATH, 'index.js');
  console.log('spawning child node process...');

  const child = spawn('node', [filePath], {stdio: 'inherit'});
  ctx.childProcess = child;
  ctx.childProcess.once('exit', (code, signal) => {
    if (!code) {
      // null or 0
      next();
    } else {
      next(`exited with error code ${code}`);
    }
  });
  ctx.childProcess.once('error', (err)=>{
    console.log('error')
    console.log(err);
    next(err);
  });

  return function stop(){
    console.log('stopping api dev')
    if(ctx.childProcess){
      // ctx.childProcess.stdin.pause();
      console.log('sending kill')
      ctx.childProcess.kill();
      ctx.childProcess = null;
    }
  }
};