output "droplet-ip" {
  value = "${digitalocean_droplet.single.ipv4_address}"
}

output "floating-ip" {
  value = "${digitalocean_floating_ip.project.ip_address}"
}