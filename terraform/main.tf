provider "digitalocean" {
}

resource "digitalocean_ssh_key" "single-droplet-ssh" {
  name       = "single-droplet-ssh"
  public_key = "${file("../ssh/id_rsa.pub")}"
}

resource "digitalocean_droplet" "single" {
  image  = "ubuntu-18-04-x64"
  name   = "web-1"
  region = "nyc1"
  size   = "s-1vcpu-1gb"

  ssh_keys = [ "${digitalocean_ssh_key.single-droplet-ssh.id}" ]

  connection {
    user        = "root"
    private_key = "${file("../ssh/id_rsa")}"
  }

  provisioner "file" {
    source      = "${path.module}/startup-script.sh"
    destination = "/root/startup-script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /root/startup-script.sh",
      "/root/startup-script.sh"
    ]
  }
}

resource "digitalocean_floating_ip" "project" {
  region = "nyc1"
}

resource "digitalocean_floating_ip_assignment" "project" {
  ip_address = "${digitalocean_floating_ip.project.id}"
  droplet_id = "${digitalocean_droplet.single.id}"
}