#!/bin/sh
# $0 is the script name, $1 is the first ARG, $2 is second...
# ./release 1.0.0-alpha.X

VERSION="$1"

git checkout -b release-$VERSION &&
npm run build &&
sed -i '' -e 's/build\///' .gitignore &&
git status &&
git add -A &&
git commit -m $VERSION &&
git push --set-upstream origin release-$VERSION &&
git checkout master &&
echo 'done'