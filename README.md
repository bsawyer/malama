![](./assets/icon-128x128-sans-circle.png)
# Malama

is an open source environmental project trying to make it easy for people to organize trash cleanups.

The name Malama comes from the Hawaiian concept *Mālama honua* which means to care for our Island Earth.
## Development

### Setup
```bash
$ npm i
```

fake keys for nginx container
```bash
$ openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout ./docker/data/certbot/conf/live/malama.cc/privkey.pem \
    -out ./docker/data/certbot/conf/live/malama.cc/fullchain.pem \
    -subj /CN=malama.cc
```

```bash
$ curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/options-ssl-nginx.conf > docker/data/certbot/conf/options-ssl-nginx.conf
$ curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem > docker/data/certbot/conf/ssl-dhparams.pem
```

### Stack

- Terraform to create Digitial Ocean resources
- Docker Compose to provision and orchestrate VPC images
- Nginx as a static file server and reverse proxy
- nodejs for a REST api
- mongodb for a temp DB

### Build

The build targets are a PWA, iOS and Android native apps, a nodejs server, and static website

## Digital Ocean

In order to run terraform commands you must export your digital ocean api key

```bash
$ cd terraform
$ source ../.digitalocean_api_token
```

The droplet will be provisioned with a set of keys.
Properly set the private key's permissions.
```bash
$ sudo chmod 400 ../ssh/id_rsa
```

Once you have run terraform plan and apply commands, you can ssh into the droplet.
`init-letsencrypt.sh` script below will create root/certbot.
```bash
$ ssh -i ../ssh/id_rsa root@<DROPLET_IP_ADDR>
$ mkdir -p /root/mongo /root/images
```

execute in /docker
```bash
$ chmod +x init-letsencrypt.sh
$ sudo ./init-letsencrypt.sh
```

## Deployment
1. `./release.sh 1.0.0-alpha.0`
2. ssh into server
3. git pull and git checkout release branch
4. cd docker && docker compose down and up

## i18n

TODO: need to pick some initial locales

## Share

Shared url's will point to the root domain `malama.cc/cleanup/:id`
which will internally proxy to the share api endpoint in order to return the appropriate HTML response.

### oEmbed

Request can be made to `https://app.malama.cc/api/oembed?url=` and returns:

```json
{
  "version": "1.0",
  "provider_url": "https://malama.cc",
  "provider_name": "Malama",
  "type": "rich",
  "title": "",
  "html": "<div style='position:relative;padding-bottom: calc(53.95% + 44px)'><iframe src='https://malama.cc/cleanup/:id' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0;' allowfullscreen></iframe></div>",
  "height": 382,
  "width": 708
}
```

## Routes

### Subdomains

#### www
- /$locale

   website index which needs to be search engine friendly

- /$locale/legal#section

- /cleanup/:id

  proxies to node docker container /api/share/:id


#### app
one index that is language agnostic. does not need to be search engine friendly. app will get language from `navigator.languages` and request the translation file from static
- /

  shows map on mobile,
  shows map and completed on anything bigger

- /cleanup

  create a cleanup

- /cleanup/$id

  shows cleanup

- /cleanup/$id/edit

  shows cleanup form

- /search

  shows search bar and list

- /search/drop-pin

  shows map with center pin and cta

- /completed

  perform /search?page=1&lat=y&lng=x will return a page of cleanups
  sorted by date and distance

- /sign-in
- /sign-up
- /password-reset
- /verify
- /reset
- /share/:id

  returns a server-side rendered CleanupView element for oEmbed iframe and sharing

### API

The api is exposed at `https://app.malama.cc/api`

- /$version/auth
- /$version/user
- /$version/user/verify
- /$version/user/password
- /$version/user/password/$token
- /$version/cleanup
- /$version/oembed?url
- /$version/share

#### cleanup status

- pending - can join start date in future and no complete date
- active - start date in past but within a day and no complete date
- complete - complete date in past
- cancelled - no start date

the user can search for a place or select lat long
and search for cleanups, not completed, with a startdate in the future, within a radius of the
so we get all the joinable cleanups within radius sorted by mongoid
`?status=pending&radius=8046.72&lat=123&lgn=1231&sort=create&start=mongoID|isodate&limit=10`
